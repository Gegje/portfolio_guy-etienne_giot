//Calculer la résistance d'une résistance en fonction des couleurs d'identification

//définition du code couleur
const valeurs = [];
valeurs.push({ couleur: 'silver', valeur: -2 });
valeurs.push({ couleur: 'gold', valeur: -1 });
valeurs.push({ couleur: 'black', valeur: 0 });
valeurs.push({ couleur: 'brown', valeur: 1 });
valeurs.push({ couleur: 'red', valeur: 2 });
valeurs.push({ couleur: 'orange', valeur: 3 });
valeurs.push({ couleur: 'yellow', valeur: 4 });
valeurs.push({ couleur: 'green', valeur: 5 });
valeurs.push({ couleur: 'blue', valeur: 6 });
valeurs.push({ couleur: 'violet', valeur: 7 });
valeurs.push({ couleur: 'grey', valeur: 8 });
valeurs.push({ couleur: 'white', valeur: 9 });
const silver = valeurs.shift();
const gold = valeurs.shift();
//initialisation des valeurs
let val1 = -3,
    val2 = -3,
    val3 = -3,
    val4 = -3;

//initialisation de la page
document.getElementsByTagName('main')[0].style.textAlign = 'center';
let begin = document.createElement('div');
begin.id = 'begin';
//affichage des titres
begin.innerHTML = '<h1 id="titre">Calcul de résistance</h1><br><h2 id="sousTitre">Cliquez pour définir les couleurs</h2><br>';
document.getElementsByTagName('main')[0].appendChild(begin);

//style pour les titres
document.getElementById('titre').style.margin = '2vh 0';
document.getElementById('titre').style.textDecoration = 'underline';
document.getElementById('sousTitre').style.margin = '1vh 0';

//création de la bande de couleurs
function createColorStrip() {
    let strip = document.createElement('div');
    strip.id = 'strip';
    strip.style.margin = '2vh 0';
    strip.style.display = 'flex';
    strip.style.flexDirection = 'row';
    strip.style.flexWrap = 'wrap';
    strip.style.width = '100vw';
    strip.style.justifyContent = 'center';
    document.getElementById('begin').appendChild(strip);

    //remplissage de la bande de couleurs associées aux valeurs, définition des propriétés
    for (i of valeurs) {
        let colorDiv = document.createElement('div');
        colorDiv.id = i.couleur;
        colorDiv.className = 'color';
        colorDiv.style.backgroundColor = i.couleur;
        colorDiv.style.width = 'auto';
        colorDiv.style.padding = '4vh';
        colorDiv.style.border = 'solid 1px black';
        colorDiv.onclick = function() {
            setValues(colorDiv.style.backgroundColor, Number(spanIn.innerHTML.replace('&nbsp;', '').replace('&nbsp;', '')))
        };
        let spanIn = document.createElement('span');
        if (i.valeur < 0) {
            spanIn.innerHTML = i.valeur + '&nbsp;';
        } else {
            spanIn.innerHTML = '&nbsp;' + i.valeur + '&nbsp;'
        };

        spanIn.style.backgroundColor = 'white';

        document.getElementById('strip').appendChild(colorDiv).appendChild(spanIn);
    }
};
createColorStrip();

//récupération des couleurs et valeurs associées, affichage des couleurs saisies (onclick sur les couleurs)
function createChoiceDiv(id, couleur, valeur) {
    let choiceDiv = document.createElement('div');
    choiceDiv.className = 'choix';
    choiceDiv.style.margin = '2vw';
    choiceDiv.style.display = 'inline-block';
    choiceDiv.innerHTML = 'couleur ' + id + ' :';

    let chosenColorDiv = document.createElement('div');
    chosenColorDiv.id = 'couleur' + id;
    chosenColorDiv.className = 'couleur';
    chosenColorDiv.style.backgroundColor = couleur;
    chosenColorDiv.style.border = 'solid 1px black';
    chosenColorDiv.style.marginTop = '2vh';
    chosenColorDiv.style.width = '4vw';
    chosenColorDiv.style.padding = '4vh';

    let spanIn = document.createElement('span');
    if (valeur < 0) {
        spanIn.innerHTML = valeur + '&nbsp;';
    } else {
        spanIn.innerHTML = '&nbsp;' + valeur + '&nbsp;'
    };
    spanIn.style.backgroundColor = 'white';

    document.getElementsByTagName('main')[0].appendChild(choiceDiv).appendChild(chosenColorDiv).appendChild(spanIn);
}

function setValues(couleur, valeur) {
    if (val1 === -3) {
        val1 = valeur;
        createChoiceDiv(1, couleur, valeur);
    } else if (val2 === -3) {
        createChoiceDiv(2, couleur, valeur);
        val2 = valeur;
    } else if (val3 === -3) {
        createChoiceDiv(3, couleur, valeur);
        val3 = valeur;
        valeurs.unshift(silver, gold);
        document.getElementById('strip').remove(document.getElementsByClassName('color'));
        createColorStrip();
    } else if (val4 === -3) {
        val4 = valeur;
        createChoiceDiv(4, couleur, valeur);
        resistance();
    } else {
        //si on clique plus de 4 fois
        alert('Pour effectuer un nouveau calcul, veuillez cliquez sur le bouton "Nouveau Calcul"');
    };
};

//adaptation de l'unité
const unit = function(resistance) {
    if (resistance > 1e9) {
        return ((resistance / 1e9 + ' gigaohm(s)'));
    } else if (resistance > 1e6) {
        return ((resistance / 1e6 + ' megaohm(s)'));
    } else if (resistance > 1e3) {
        return ((resistance / 1e3 + ' kiloohm(s)'));
    } else {
        return (resistance + ' ohm(s)');
    }
}

//calcul et affichage de la résistance
function resistance() {
    resistance = unit(Math.round((val1 * 100 + val2 * 10 + val3) * Math.pow(10, val4) * 100) / 100);
    const resultat = document.createElement('div');
    resultat.id = 'resultat';
    resultat.style.marginTop = '5vh';
    resultat.style.fontSize = '2em';
    resultat.innerHTML = 'La résistance calculée est de :<br>' + resistance + '<br>';
    document.getElementsByTagName('main')[0].appendChild(resultat);
    const button = document.createElement('button');
    button.id = 'reload';
    button.innerHTML = '<a href="#" onclick="document.location.reload()">Nouveau Calcul</a>';
    button.style.margin = '5vh 0';
    button.style.padding = '1em';
    document.getElementsByTagName('main')[0].appendChild(button);


}