<?php
include("templates/DB.php");
include('templates/funks.php');
session_start();
$favorites = findFavoritesDetails($_SESSION['user']['id']);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Favoris</title>
    <link rel="stylesheet" href="css/app_css.css">
</head>

<body>
    <?php include('templates/header.php') ?>
    <main>
        <section class="movies-list">
            <?php
            //s'il y en a,
            //on affiche les films dans la liste de favoris
            if (!empty($favorites)) {
                foreach ($favorites as $favorite) {
                    echo '<a href="details.php?id=' . $favorite['id'] . '"';
                    echo 'title="' . $favorite['title'] . '">';
                    echo '<img src="img/posters/' . $favorite['image'] . '" alt="' . $favorite['title'] . '"><br>';
                    echo "<h3>" . $favorite['title'] . "</h3>";
                    echo '</a>';
                }
            } else {
                echo '<h4>Vous n\'avez encore aucun favori!<br>Faites une recherche ou retournez à l\'accueil!</h4>';
            }
            ?>
        </section>
    </main>
</body>

</html>