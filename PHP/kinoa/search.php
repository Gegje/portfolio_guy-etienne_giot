<?php
include("templates/DB.php");
include('templates/funks.php');
session_start();

//si je n'ai pas de g dans l'URL...
if (empty($_GET['search'])) {
    error404();
} else {
    $movie_datas = search();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Page de recherche</title>
    <link rel="stylesheet" href="css/app_css.css">

</head>

<body>
    <?php include('templates/header.php') ?>
    <main>
        <?php echo '<h2>Results für deine recherche (' . str_replace("+", " ",$_GET['search']) . ')</h2>'; ?>
        <section class="movies-list">
            <?php
            //pour identifier le film recherché en détails, on passe son id dans l'url
            foreach ($movie_datas as $movie_data) {
                if (!empty($_SESSION['user'])) {
                    echo '<a href="details.php?id=' . $movie_data['id'] . '"';
                    echo 'title="' . $movie_data['title'] . '">';
                    echo '<img src="img/posters/' . $movie_data['image'] . '" alt="' . $movie_data['title'] . '"><br>';
                    echo "<h3>" . $movie_data['title'] . "</h3>";
                    echo '</a>';
                } else {
                    echo '<a href="login.php?id=' . $movie_data['id'] . '"';
                    echo 'title="' . $movie_data['title'] . '">';
                    echo '<img src="img/posters/' . $movie_data['image'] . '" alt="' . $movie_data['title'] . '"><br>';
                    echo "<h3>" . $movie_data['title'] . "</h3>";
                    echo '</a>';
                }
            }
            ?>
        </section>
    </main>
</body>

</html>