<?php
include("templates/DB.php");
include('templates/funks.php');
session_start();

$movie_datas = findMovieDatas();
//si l'id n'était pas valide
if (empty($movie_datas)) {
    error404();
}

$favorites = findFavoritesList($_SESSION['user']['id']);
//si je n'ai pas d'id dans l'URL...
if (empty($_GET['id'])) {
    error404();
}
//vérifie si le film est déjà dans les favoris
$like = 0;
foreach ($favorites as $favorite) {
    if ($favorite['movie_id'] == $movie_datas['id']) {
        $like = 1;
        break;
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Détails</title>
    <link rel="stylesheet" href="css/app_css.css">

</head>

<body>
    <?php include('templates/header.php') ?>
    <main class="movie-detail">

        <div class="left-col">
            <h2><?php echo $movie_datas['title']; ?> (<?php echo $movie_datas['year'] ?>)</h2>
            <img src="img/posters/<?php echo $movie_datas['image']; ?>" alt="<?php echo $movie_datas['title']; ?>">
            <div>
                <?php
                //on sépare chaque catégorie... et on les récupère dans un tableau
                $genres = explode(" / ", $movie_datas['genres']);
                //on boucle sur le tableau pour afficher chaque genre dans sa propre div
                foreach ($genres as $genre) {
                    echo '<a href="genres.php?g=' . $genre . '" class="badge">' . $genre . '</a>';
                }
                ?>
            </div>
            <div>
                <h3> IMDB Rating</h3>
                <?php
                //floor arrondit le rating à l'entier inférieur
                //on pourrait utiliser rounded() ou ceil() aussi...
                for ($i = 0; $i < floor($movie_datas['rating']); $i++) {
                    echo "⭐";
                }
                for ($i = floor($movie_datas['rating']); $i < 10; $i++) {
                    echo "☆";
                }
                ?>
            </div>
            <?php if ($like == 0) {
                echo '<a href="favorite.php?id=' . $movie_datas['id'] . '"><h3>Ajouter à ma liste</h3></a>';
            }
            ?>
        </div>
        <div class="col">
            <h2><?php echo $movie_datas['plot']; ?></h2>
            <div class="movie-details">
                <h3>Réalisé par <p><?php echo $movie_datas['directors']; ?></p>
                </h3>


                <h3>Starring : <p><?php echo str_replace(" / ", ", ", $movie_datas['actors']); ?></p>
                </h3>


                <h3>Spieldauer : <p>
                        <?php
                        //transforme le runtime (en minutes) au format h min
                        $hours = floor($movie_datas['runtime'] / 60);
                        $minutes = $movie_datas['runtime'] % 60;
                        echo "$hours h $minutes min";
                        ?>
                    </p>
                </h3>
            </div>

            <div> <?php echo '<iframe width="560" height="315" src="https://www.youtube.com/embed/' . $movie_datas['trailer_id'] . '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>' ?> </div>
        </div>
    </main>
</body>

</html>