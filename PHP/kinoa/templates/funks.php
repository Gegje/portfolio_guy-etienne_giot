<?php

function newPdo()
{
    try {
        //connexion à la base avec la classe PDO et le dsn
        $pdo = new PDO('mysql:host=' . DBHOST . ';dbname=' . DBNAME . ';charset=UTF8', DBUSER, DBPASS, array(
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, //on récupère nos données en array associatif par défaut
            PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING         //on affiche les erreurs. À modifier en prod. 
        ));
    } catch (PDOException $e) { //attrappe les éventuelles erreurs de connexion
        echo 'Erreur de connexion : ' . $e->getMessage();
    }
    return $pdo;
}

function find30Movies()
{
    //connection à la bdd
    $pdo = newPdo();

    // pour récupérer 30 films au hasard
    $sql = "SELECT *
            FROM movies
            ORDER BY RAND()
            LIMIT 30";

    $stmt = $pdo->prepare($sql);
    $stmt->execute();

    //récupère le résultat de la requête depuis le serveur
    $movies = $stmt->fetchALL();
    return $movies;
}

function findMovieDatas()
{
    $pdo = newPdo();

    //récupère l'id du film présent dans l'url
    // strip_tags enlève les balises html pour éviter les attaques XSS
    $movieId = strip_tags($_GET['id']);

    // pour récupérer les données du film
    $sql = "SELECT *
        FROM movies
        WHERE id= :movieId";

    $stmt = $pdo->prepare($sql);
    $stmt->execute([
        ":movieId" => $movieId
    ]);

    // récupère les données
    $movie_datas = $stmt->fetch();
    return $movie_datas;
}


function register()
{
    //connection à la bdd
    $pdo = newPdo();

    //si on a des données dans $_POST, 
    //c'est que le form a été soumis
    if (!empty($_POST)) {
        //par défaut, on dit que le formulaire est entièrement valide
        //si on trouve ne serait-ce qu'une seule erreur, on 
        //passera cette variable à false
        // strip_tags enlève les balises html pour éviter les attaques XSS

        $formIsValid = true;

        $email = strip_tags($_POST['email']);
        $lastname = strip_tags($_POST['lastname']);
        $firstname = strip_tags($_POST['firstname']);
        $password = $_POST['password'];
        $age = strip_tags($_POST['age']);

        //tableau qui stocke nos éventuels messages d'erreur
        $errors = [];

        //si le lastname est vide...
        if (empty($lastname)) {
            //on note qu'on a trouvé une erreur ! 
            $formIsValid = false;
            $errors[] = "Veuillez renseigner votre nom de famille !";
        }
        //mb_strlen calcule la longueur d'une chaîne
        elseif (mb_strlen($lastname) <= 1) {
            $formIsValid = false;
            $errors[] = "Votre nom de famille est court, très court. Veuillez le rallonger !";
        } elseif (mb_strlen($lastname) > 30) {
            $formIsValid = false;
            $errors[] = "Votre nom de famille est trop long !";
        }

        //validation du firstname
        if (empty($firstname)) {
            $formIsValid = false;
            $errors[] = "Veuillez renseigner votre prénom !";
        } elseif (mb_strlen($firstname) <= 1) {
            $formIsValid = false;
            $errors[] = "Votre prénom est court, très court. Veuillez le rallonger !";
        } elseif (mb_strlen($firstname) > 30) {
            $formIsValid = false;
            $errors[] = "Votre prénom est trop long !";
        }

        //validation de l'email
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $formIsValid = false;
            $errors[] = "Votre email n'est pas valide !";
        }

        //validation de l'âge
        if ($age < 5) {
            $formIsValid = false;
            $errors[] = "Tu es trop jeune, attends encore un peu.";
        } elseif ($age > 130) {
            $formIsValid = false;
            $errors[] = "Vous êtes le doyen de l'humanité?";
        }
        //si on a  reçu autre chose qu'un nombre
        elseif (!is_numeric($age)) {
            $formIsValid = false;
            $errors[] = "Votre âge en chiffres please !";
        }

        //si le formulaire est toujours valide... 
        if ($formIsValid == true) {
            $sql = "INSERT INTO users 
            (firstname, lastname, email, password, age, date_created)
            VALUES 
            (:firstname, :lastname, :email, :password, :age, NOW())";

            $stmt = $pdo->prepare($sql);
            $stmt->execute([
                ":firstname" => $firstname,
                ":lastname" => $lastname,
                ":email" => $email,
                ":password" => $password,
                ":age" => $age,
            ]);

            //connexion du user
            $_SESSION['user']['id'] = $pdo->lastInsertId();

            //on redirige l'utilisateur où il voulait aller, sinon vers l'accueil (404 si id étrange) 
            if (!empty($_GET['id'])) {
                if (is_numeric($_GET['id']) and $_GET['id'] > 0) {
                    header("Location: details.php?id=" . $_GET['id']);
                } elseif ($_GET['id'] == 0) {
                    header("Location: index.php");
                } else {
                    error404();
                }
            } else {
                header("Location: index.php");
                die();
            }
        }//renvoie le tableau d'erreurs
    return $errors;
    } 
}


function findUserByEmail($email)
{
    $sql = "SELECT * FROM users 
                WHERE email = :email";

    //récupère notre connexion à la bdd
    $pdo = new PDO('mysql:host=' . DBHOST . ';dbname=' . DBNAME . ';charset=UTF8', DBUSER, DBPASS);

    $stmt = $pdo->prepare($sql);
    $stmt->execute([
        ":email" => $email,
    ]);

    $foundUser = $stmt->fetch();
    return $foundUser;
}


function login()
{
    $pdo = newPdo();
    if (!empty($_POST)) {
        //par défaut je dis que ce n'est pas valide
        $formIsValid = false;

        //récupére le username ou l'email 
        $email = $_POST['email'];
        //récupére le mot de passe du form
        $password = $_POST['password'];

        //va chercher dans la bdd la ligne correspondant à l'email
        $foundUser = findUserByEmail($email);

        //si on a trouvé un user avec cet email...
        if (!empty($foundUser)) {
            //si le mdp matche
            if ($foundUser['password'] === $password) {

                //connexion du user !!!!
                $_SESSION['user'] = $foundUser;

                //validation du formulaire
                $formIsValid = true;

                //redirige l'utilisateur où il voulait aller, sinon vers l'accueil (404 si id étrange) 
                if (!empty($_GET['id'])) {
                    if (is_numeric($_GET['id'])) {
                        header('Location: details.php?id=' . $_GET['id'] . '');
                    } else {
                        error404();
                    }
                } else {
                    header("Location: index.php");
                    die();
                }
            }
        }return $formIsValid;
    } 
}


function findMoviesByGenre($genre)
{
    $pdo = newPdo();
    // pour récupérer les données du film
    $sql = "SELECT *
            FROM movies
            WHERE genres LIKE :genre";

    $stmt = $pdo->prepare($sql);
    $stmt->execute([
        ":genre" =>'%'.$genre.'%',
    ]);

    // récupère les données
    $movies = $stmt->fetchAll();
    return $movies;
}
function search()
{
    $pdo = newPdo();
    // strip_tags enlève les balises html pour éviter les attaques XSS
    $query = strip_tags($_GET['search']);
    $search = '%' . str_replace(" ", "%", $query) . '%';

    $sql = "SELECT id, title, image, plot FROM movies 
            WHERE title LIKE :search OR actors LIKE :search OR directors LIKE :search";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([
        ":search" => $search
    ]);

    $movie_datas = $stmt->fetchAll();
    return $movie_datas;
}


function addToFavorites($userId, $movieId)
{
    $pdo = newPdo();

    $sql = "INSERT INTO favorites (user_id, movie_id)
            VALUES (:userId, :movieId)";

    $pdo = newPdo();

    $stmt = $pdo->prepare($sql);
    $stmt->execute([
        ":userId" => $userId,
        ":movieId" => $movieId,
    ]);
}

function findFavoritesList($userId)
{
    $sql = "SELECT * FROM favorites WHERE user_id = :userId";

    $pdo = new PDO('mysql:host=' . DBHOST . ';dbname=' . DBNAME . ';charset=UTF8', DBUSER, DBPASS);

    $stmt = $pdo->prepare($sql);
    $stmt->execute([
        ":userId" => $userId,
    ]);

    $favorites = $stmt->fetchAll();
    return $favorites;
}

function findFavoritesDetails($userId)
{
    $sql = "SELECT * FROM favorites JOIN movies ON favorites.movie_id = movies.id HAVING favorites.user_id = :userId";

    $pdo = newPdo();

    $stmt = $pdo->prepare($sql);
    $stmt->execute([
        ":userId" => $userId,
    ]);

    $favorites = $stmt->fetchAll();
    return $favorites;
}

function debug($var)
{
    //la balise <pre> permet de conserver l'indentation
    echo '<pre style="background-color: #000; color: lightgreen; padding: 10px;">';
    print_r($var);
    echo '</pre>';
}


function error404()
{ {
        //on redirige l'internaute vers la page d'erreur
        //on envoie un code d'erreur 404
        header("HTTP/1.0 404 Not Found");
        //on affiche notre fichier contenant le code HTML
        include("404.php");
        //on arrête ici l'exécution du script ! 
        //on ne veut pas que le reste du code soit lu
        die();
    }
}
