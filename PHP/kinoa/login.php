<?php
include("templates/DB.php");
include("templates/funks.php");
session_start();
$formIsValid = login();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connexion</title>
    <link rel="stylesheet" href="css/app_css.css">
</head>

<body>
    <header>
        <h1><a href="index.php">Kinoa!!! &nbsp; The Kino for toi!</a></h1>
    </header>
    <h1>Connectez-vous <?php if (!empty($_GET['id'])) {
                            echo ' pour voir les détails ';
                        } ?> !</h1>

    <!--formulaire de login -->
    <div class="container">
        <div class="content">
            <form  class=" login" method="post">
                <div class="item">
                    <label for="email">Votre email</label>
                    <input type="text" name="email" id="email">
                </div>

                <div class="item">
                    <label for="password">Votre mot de passe</label>
                    <input type="password" name="password" id="password">
                </div>

                <?php
                //éventuel message d'erreur
                if (isset($formIsValid) && $formIsValid === false) {
                    echo '<div class="alert">Veuillez remplir correctement le formulaire!</div>';
                }
                ?>

                <button>C'est parti !</button>

                <!--si l'utilisateur n'est pas encore inscrit, on lui propose de le faire-->
            </form>
            <p>Pas encore de compte?&nbsp;<a href="register.php?id=
            <?php
                if (!empty($_GET['id'])) {
                    echo $_GET['id'];
                } else {
                    echo '0';
                } ?>"><button>Je m'inscris</button></a></p>
        </div>
    </div>
</body>

</html>