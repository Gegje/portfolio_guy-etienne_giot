<?php
include("templates/DB.php");
include("templates/funks.php");
session_start();

$errors = register();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inscription</title>
    <link rel="stylesheet" href="css/app_css.css">
</head>

<body>
    <header>
        <h1><a href="index.php">Kinoa!!! &nbsp; The Kino for toi!</a></h1>
    </header>
    <h1>Créez votre compte !</h1>

    <!-- seuls les formulaires de recherche doivent être en get -->
    <div class="container">
        <div class="content">
            <form method="post">
                <div class="item">
                    <label for="lastname">Votre nom</label>
                    <input type="text" name="lastname" id="lastname">
                </div>
                <div class="item">
                    <label for="firstname">Votre prénom</label>
                    <input type="text" name="firstname" id="firstname">
                </div>
                <div class="item">
                    <label for="email">Votre email</label>
                    <input type="email" name="email" id="email">
                </div>
                <div class="item">
                    <label for="password">Votre mot de passe</label>
                    <input type="password" name="password" id="password">
                </div>
                <div class="item">
                    <label for="age">Votre âge</label>
                    <input type="number" name="age" id="age">
                </div>


                <?php
                //affiche les éventuelles erreurs de validations
                if (!empty($errors)) {
                    foreach ($errors as $error) {
                        echo '<div class="alert">' . $error . '</div>';
                    }
                }
                ?>
                <button>Envoyer !</button>
            </form>
        </div>
    </div>
</body>

</html>