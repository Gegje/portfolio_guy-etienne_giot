<?php
//inclue les constantes de la bdd
include("templates/DB.php");
//inclue le fichier contenant toutes les fonctions
include("templates/funks.php");

session_start();
 
$movies = find30Movies()

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Accueil</title>
    <link rel="stylesheet" href="css/app_css.css">
</head>

<body>
    <?php include('templates/header.php') ?>
    <main>
        <section class="movies-list">
            <?php
            //on veut afficher 30 films
            //pour identifier le film recherché en détails, on passe son id dans l'url
            foreach ($movies as $movie) {
                if (!empty($_SESSION['user'])){
                echo '<a href="details.php?id=' . $movie['id'] . '"';
                echo 'title="' . $movie['title'] . '">';
                echo '<img src="img/posters/' . $movie['image'] . '" alt="' . $movie['title'] . '"><br>';
                echo "<h3>" . $movie['title'] . "</h3>";
                echo '</a>';
            } else {
                echo '<a href="login.php?id='.$movie['id'].'"';
                echo 'title="' . $movie['title'] . '">';
                echo '<img src="img/posters/' . $movie['image'] . '" alt="' . $movie['title'] . '"><br>';
                echo "<h3>" . $movie['title'] . "</h3>";
                echo '</a>';
            }
        }
            ?>
        </section>
    </main>
</body>

</html>