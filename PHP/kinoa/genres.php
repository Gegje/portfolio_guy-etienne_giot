<?php
include("templates/DB.php");
include("templates/funks.php");
session_start();

//si je n'ai pas de g dans l'URL...
if (empty($_GET['g'])) {
    error404();
}

//récupère l'id du film présent dans l'url 
// strip_tags enlève les balises html pour éviter les attaques XSS
$genre = strip_tags($_GET['g']);

$movies = findMoviesByGenre($genre);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Par Genre</title>
    <link rel="stylesheet" href="css/app_css.css">

</head>

<body>
<?php include('templates/header.php') ?>
    <main>
        <?php echo '<h2>Unsere selection en  " ' . $genre . ' "</h2>'; ?>
        <section class="movies-list">
            <?php
            //pour identifier le film recherché en détails, on passe son id dans l'url
            foreach ($movies as $movie) {
                echo "<div>";
                echo '<a href="details.php?id=' . $movie['id'] . '" title="' . $movie['title'] . '">';
                echo '<img src="img/posters/' . $movie['image'] . '" alt="' . $movie['title'] . '"><br>';
                echo "<h3>" . $movie['title'] . "</h3>";
                echo '</a>';
                echo "<h3>";
                for ($i = 0; $i < floor($movie['rating']); $i++) {
                    echo "⭐";
                }
                for ($i = floor($movie['rating']); $i < 10; $i++) {
                    echo "☆";
                }
                echo "</h3>";
                echo "<br>";
                echo "</div>";
            }
            ?>
        </section>
    </main>
</body>

</html>