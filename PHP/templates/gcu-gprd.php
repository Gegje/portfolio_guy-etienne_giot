<h1>INTRODUCTION</h1>
<br>

<p>Thank you for your interest on this pages and for the time you offered me. I am really committed to protecting your personal information and your right to privacy. If you have any questions or concerns about my policy, or my practices with regards to your personal information, please contact me at guy-etienne.giot@students.campus.academy.<br>
    <br>
    When you visit my website https://www.trouve.cf/guyetiennegiot (“Site”) and use my services, you trust me with your personal information. I take your privacy very seriously. In this privacy notice, I describe my privacy policy. I seek to explain to you in the clearest way possible what information I collect, how I use it and what rights you have in relation to it. I hope you take some time to read through it carefully, as it is important. If there are any terms in this privacy policy that you do not agree with, please discontinue use of my site and my services.<br>
    <br>
    This privacy policy applies to all information collected through my website (https://www.trouve.cf/guyetiennegiot).<br>
    <b>Please read this privacy policy carefully as it will help you make informed decisions about sharing your personal information with me.</b> </p>

<br>
<h2>Table of contents</h2>
<ol>
    <li>WHAT INFORMATION DO I COLLECT? </li>
    <li>HOW DO I USE YOUR INFORMATION? </li>
    <li>WILL YOUR INFORMATION BE SHARED WITH ANYONE? </li>
    <li>DO I USE COOKIES AND OTHER TRACKING TECHNOLOGIES? </li>
    <li>IS YOUR INFORMATION TRANSFERRED INTERNATIONALLY? </li>
    <li>HOW LONG DO I KEEP YOUR INFORMATION? </li>
    <li>HOW DO I KEEP YOUR INFORMATION SAFE? </li>
    <li>DO I COLLECT INFORMATION FROM MINORS? </li>
    <li>WHAT ARE YOUR PRIVACY RIGHTS? </li>
    <li>DO I MAKE UPDATES TO THIS POLICY? </li>
    <li>HOW CAN YOU CONTACT US ABOUT THIS POLICY? </li>
</ol>



 <br>

<H2>WHAT INFORMATION DO I COLLECT? </H2>
<br>

<h3>Personal information you disclose to me </h3>

<p>In Short: I collect personal information that you provide to me such as name, entreprise, contact information.<br>
    I collect personal information that you voluntarily provide to me when giving a recommendation, expressing an interest in obtaining information about me or my services or otherwise contacting me. <br>
    The personal information that I collect depends on the context of your interactions with me and the Site. The personal information I COLLECT can include the following: <br></p>
<h4>Name and Contact Data.</h4>
<p> I collect your first and last name, email address and eventually enterprise. I also keep your IP and your browser.<br>
    All personal information that you provide to me must be true, complete and accurate, and you must notify me of any changes to such personal information. </p>
<br>

<H2>HOW DO I USE YOUR INFORMATION? </H2>
<br>

<p>In Short: I process your information for purposes based on legitimate business interests, compliance with my legal obligations and/or your consent.
    <br>
    I use personal information collected via my site only for keeping your contact information and keeping in touch with you!! Your IP and browser information are for statistic purposes only</p>
<br>

<H2>WILL YOUR INFORMATION BE SHARED WITH ANYONE? </H2>
<br>

<p>In Short: I don’t share information.
    <br>
    I will never share or disclose your informations!</p>
<br>

<H2>DO I USE COOKIES AND OTHER TRACKING TECHNOLOGIES? </H2>
<br>

<p>In Short: No, I don’t.
    <br>
    I don’t use cookies or similar tracking technologies to access or store information.</p>
<br>

<H2>IS YOUR INFORMATION TRANSFERRED INTERNATIONALLY? </H2>
<br>

<p>In Short: I may transfer, store, and process your information in countries other than your own.
    <br>
    Our servers are located in France. If you are accessing my sites from outside France, please be aware that your information may be transferred to, stored, and processed by me in my facilities. <br>
    If you are a resident in the European Economic Area, then these countries may not have data protection or other laws as comprehensive as those in your country. I will however take all necessary measures to protect your personal information in accordance with this privacy policy and applicable law. </p>

<br>

<H2>HOW LONG DO I KEEP YOUR INFORMATION? </H2>
<br>

<p>In Short: I keep your information for as long as necessary to fulfill the purposes outlined in this privacy policy unless otherwise required by law.
    <br>
    We will only keep your personal information for as long as it is necessary for the purposes set out in this privacy policy, unless a longer retention period is required or permitted by law (such as tax, accounting or other legal requirements). Please contact me at guy-etienne.giot@students.campus.academy if you don’t want your informations being kept any longer, I will either delete or anonymize it, or, if this is not possible (for example, because your personal information has been stored in backup archives), then I will securely store your personal information and isolate it from any further processing until deletion is possible.</p>
 
<H2>HOW DO I KEEP YOUR INFORMATION SAFE? </H2>
<br>

<p>In Short: I aim to protect your personal information through a system of organizational and technical security measures.
    <br>
    I have implemented appropriate technical and organizational security measures designed to protect the security of any personal information I process. However, please also remember that I cannot guarantee that the internet itself is 100% secure. Although I will do my best to protect your personal information, transmission of personal information to and from my Sites is at your own risk. You should only access the services within a secure environment. </p>
<br>

<H2>DO I COLLECT INFORMATION FROM MINORS? </H2>
<br>
<p>In Short: I do not knowingly collect data from or market to children under 18 years of age.
    <br>
    We do not knowingly solicit data from or market to children under 18 years of age. By using the site, you represent that you are at least 18 or that you are the parent or guardian of such a minor and consent to such minor dependent’s use of the site. If I learn that personal information from users less than 18 years of age has been collected, I will take reasonable measures to promptly delete such data from my records. If you become aware of any data I have collected from children under age 18, please contact me at guy-etienne.giot@students.campus.academy. </p>
<br>

<H2>WHAT ARE YOUR PRIVACY RIGHTS?</H2>
<br>

<p>In Short: You may review, change, or terminate your account at any time.
    <br>
    In some regions (like the European Economic Area), you have certain rights under applicable data protection laws. These may include the right</p>
<ul>
    <li> to request access and obtain a copy of your personal information,</li>
    <li> to request rectification or erasure </li>
    <li> to restrict the processing of your personal information and </li>
    <li> if applicable, to data portability.</li>
</ul>
<p> In certain circumstances, you may also have the right to object to the processing of your personal information. To make such a request, please use the contact details provided on the site . I will consider and act upon any request in accordance with applicable data protection laws. </p>

<p>If I are relying on your consent to process your personal information, you have the right to withdraw your consent at any time. Please note however that this will not affect the lawfulness of the processing before its withdrawal.
    <br>
    If you are resident in the European Economic Area and you believe I am unlawfully processing your personal information, you also have the right to complain to your local data protection supervisory authority. You can find their contact details here: http://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm</p>
<br>

<H2>DO I MAKE UPDATES TO THIS POLICY?</H2>
<br>

<p>In Short: Yes, I will update this policy as necessary to stay compliant with relevant laws.
    <br>
    I may update this privacy policy from time to time. The updated version will be effective as soon as it is accessible. I encourage you to review this privacy policy frequently to be informed of how I am protecting your information. </p>
<br>

<H2>HOW CAN YOU CONTACT US ABOUT THIS POLICY?</H2>
<br>

<p>If you have questions or comments about this policy, email me at guy-etienne.giot@students.campus.academy or by post to:<br>
    <br>
    Guy-Etienne GIOT<br>
    1 allée Arthur Christiansen<br>
    44230 SAINT-SEBASTIEN SUR LOIRE<br>
    FRANCE<br>
</p>