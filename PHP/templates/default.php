<!-- page commune pour les contenus de premier niveau
    
include le haut de page -->
<?php include("templates/top.php");?>

<div class="mainbox conteneur">
    <?php
    //affiche les contenus récupérés en base de données (depuis le Controller et le Manager)
    foreach ($contents as $content) {
        echo '<div class="contenu">
                <div class="mini">
                    <a href="' . $content['link'] . '" target="_blank"><img src="images/' . $content['picture'] . '"></a>
                </div>
                <a href="' . $content['link'] . '" target="_blank"><div class="resume">
                ' . $content['content'] . '</div></a>
                
            </div>';
    }
    ?>
</div>
<?php include("templates/bottom.php"); ?>