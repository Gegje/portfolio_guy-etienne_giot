<!--affiche les détails d'expériences ou de projets-->

<?php include("templates/top.php");
if ($_SESSION['lang'] == 'fr') {
    echo '<p>N\'hésitez pas à cliquer sur les images pour plus de détails</p>';
} else {
    echo '<p> Please click on the pictures for more details</p>';
}
?>

<div class="mainbox2">
    <?php
    //bouton de gauche, vide si pas de navigation vers la gauche
    if ($_GET['id'] > 0) {
        echo '<a href="index.php?page=details&title=' . $_GET['title'] . '&id=' . ($_GET['id'] - 1) . '&lang=' . $_SESSION['lang'] . '">'
        ?>
        <img class="arrows" src="images/arrows/back-backwards-repeat-gold.png" onmouseover="this.src='images/arrows/back-backwards-repeat-white.png'" onmouseout="this.src='images/arrows/back-backwards-repeat-gold.png'">
                </a>
    <?php
    } else {
        echo '<button type="button"></button>';
    }

    //récupére le contenu à afficher
    $content = $contents[$_GET['id']];
    
    if ($content['link']) {?>

    <a href="<?php echo $content['link'] ?>" target="_blank">
        <img src="images/<?php echo $content['picture'] ?>" alt="<?php echo $content['alt'] ?>">
    </a>
    <?php
    }
    else {?>
    <img src="images/<?php echo $content['picture'] ?>" alt="<?php echo $content['alt'] ?>">
    <?php
    }

    //bouton de droite, vide si pas de navigation vers la droite
    if ($_GET['id'] < (count($contents) - 1)) {
        echo '<a href="index.php?page=details&title=' . $_GET['title'] . '&id=' . ($_GET['id'] + 1) . '&lang=' . $_SESSION['lang'] . '">'
        ?>
        <img class="arrows" src="images/arrows/forward-arrows-arrow-front-go_gold.png" onmouseover="this.src='images/arrows/forward-arrows-arrow-front-go_white.png'" onmouseout="this.src='images/arrows/forward-arrows-arrow-front-go_gold.png'">
        </a>
        <?php
        if (!empty($_SESSION['user'])) {
            unset($_SESSION['user']);
        }
    } elseif ( $_GET['title'] == 'portfolio'){
        echo '<a href="index.php?page=404">'
        ?>
        <img class="arrows" src="images/arrows/forward-arrows-arrow-front-go_gold.png" onmouseover="this.src='images/arrows/forward-arrows-arrow-front-go_white.png'" onmouseout="this.src='images/arrows/forward-arrows-arrow-front-go_gold.png'">
        </a><?php
    } else {
        echo '<button type="button"></button>';
        if (!empty($_SESSION['user'])) {
            unset($_SESSION['user']);
        }
    }
    ?>
    <div class="description">
        <?php echo $content['content']; ?>
    </div>
    <!--fermeture de la page-->
    <div class="back">
        <?php
        if ($_SESSION['lang'] == 'fr') {
            echo '<a href="javascript:self.close();">Fermer</a>';
        } else {
            echo '<a href="javascript:self.close();">Close</a>';
        } ?>
    </div>
</div>
<?php include('templates/bottom.php'); ?>