<?php
//récupère l'adresse de la page en cours
if (isset($_GET['id']) & isset($_GET['title'])) {
    $pageName = 'index.php?page=' . $_GET['page'] . '&title=' . $_GET['title'] . '&id=' . $_GET['id'] . '&lang=' . $_SESSION['lang'];
}elseif (isset($_GET['title'])){
    $pageName = 'index.php?page=' . $_GET['page'] . '&title=' . $_GET['title'] . '&lang=' . $_SESSION['lang'];
}elseif (isset($_GET['id'])){
    $pageName = 'index.php?page=' . $_GET['page'] . '&id=' . $_GET['id'] . '&lang=' . $_SESSION['lang'];
}else{
    $pageName = 'index.php?page=' . $_GET['page'] . '&lang=' . $_SESSION['lang'];
}

//crée une nouvelle visite sur la page
$visit = new Visit($pageName);

//récupère le nombre de visites sur la page
$visitsCount = Manager::countVisits($pageName);

//prépare les liens internes et les éventuels changement de langue
if (isset($_GET['title'])) {
    
    if ($_SESSION['lang'] == 'en') {
        if ($_GET['title'] == 'educational_background') {
            $page = substr(str_replace("educational_background", "formations", $pageName),0,-8);
        } elseif ($_GET['title'] == 'school_projects') {
            $page = substr(str_replace("school_projects", "projets_scolaires", $pageName),0,-8);
        }  else{
            $page = substr($pageName,0, -8) ;
        }
    } elseif ($_SESSION['lang'] == 'fr') {
        if ($_GET['title'] == 'formations') {
            $page = substr(str_replace("formations", "educational_background", $pageName),0,-8);
        } elseif ($_GET['title'] == 'projets_scolaires') {
            $page = substr(str_replace("projets_scolaires", "school_projects", $pageName),0,-8);
        } else{
            $page = substr($pageName,0, -8) ;
        }
    }
    //récupère le titre de la page pour affichage
    $pageTitle = str_replace("_", " ", ucwords($_GET['title']));

} else{
    $page = substr($pageName,0, -8) ;
    $pageTitle = $_GET['page'];
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="index,follow">
    <meta name="google-site-verification" content="4flAVFYusAxFeeOI4bVZgyLIHv30_Nayp4NQ221HYN0" />
    <meta name="msvalidate.01" content="C8CF9F5E3A5DE46F7EC079DC1DEC9EE0" />
    <meta name="description" content="portfolio de Guy-Etienne Giot, étudiant en Bachelor Informatique à Nantes: formations, expériences et projets, contact">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Guy-Etienne Giot -&nbsp;<?php echo $pageTitle ?></title>
    <link rel="stylesheet" href="css/menu.css">
    <link rel="stylesheet" href="css/default.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet"> 
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
     <script>
       function onSubmit(token) {
         document.getElementById("form").submit();
       }
     </script>


    <!--inclut les css spécifiques et les éventuels scripts selon les pages-->
    <?php
    if ($_GET['page'] == 'details') {
        echo '<link rel="stylesheet" href="css/details.css">';
    } elseif ($_GET['page'] == 'stage') {
        echo '<link rel="stylesheet" href="css/stage.css">';
    } elseif ($_GET['page'] == 'contacts') {
        echo '<link rel="stylesheet" href="css/form.css">';
    } elseif ($_GET['page'] == 'cgu') {
        echo '<link rel="stylesheet" href="css/mentions.css">';
    } elseif ($_GET['page'] == '404') {
        echo '<link rel="stylesheet" href="css/404.css">';
    }
    ?>

</head>

<body>
    <header class="conteneur">
    <?php include("templates/nav.php");?>
    <h1>Guy-Etienne Giot</h1>
    <div class="lang">
        <?php if ($_SESSION['lang']=='en'){
        echo '
            <a href="'. $page . '&lang=fr">
                <img class="lang-radio" src="images/fr.png">
            </a>';
        } else {
            echo '<a href="'. $page . '&lang=en">
                <img class="lang-radio" src="images/eng.png">
            </a>';
        }?>
    </div>
    </header>
    <main>
        <?php
        if ($_GET['page'] == 'default' || $_GET['page'] == 'contacts' || $_GET['page'] == 'cgu') {
            echo '<h1 class="titre">' . $pageTitle . '</h1>';
        }
        ?>