<!--affiche les détails d'expériences ou de projets-->

<?php include("templates/top.php") ?>
<h1>
    <?php
    if ($_GET['lang'] == 'fr') {
        echo 'Stage chez Toolin';
    } else {
        echo 'Internship at Toolin';
    }
    ?>
</h1>
<div class="mainbox2">
    <?php
    //récupére le contenu à afficher
    $content = $contents[$_GET['id']];
    ?>
    <div class="description">
        <?php echo $content['content']; ?>
    </div>
</div>
<div id="navigation">
    <?php
    //bouton de gauche, vide si pas de navigation vers la gauche
    if ($_GET['id'] > 0) {
        echo '<a href="index.php?page=stage&id=' . ($_GET['id'] - 1) . '&lang=' . $_SESSION['lang'] . '">'
    ?>
        <img class="arrows" src="images/arrows/back-backwards-repeat-gold.png" onmouseover="this.src='images/arrows/back-backwards-repeat-white.png'" onmouseout="this.src='images/arrows/back-backwards-repeat-gold.png'">
        </a>
    <?php
    } else {
        echo '<button type="button"></button>';
    }
    ?>
    <!--fermeture de la page-->
    <div class="back">
        <?php
        if ($_SESSION['lang'] == 'fr') {
            echo '<a href="javascript:self.close();">Fermer</a>';
        } else {
            echo '<a href="javascript:self.close();">Close</a>';
        } ?>
    </div>
    <?php
    //bouton de droite, vide si pas de navigation vers la droite
    if ($_GET['id'] < (count($contents) - 1)) {
        echo '<a href="index.php?page=stage&id=' . ($_GET['id'] + 1) . '&lang=' . $_SESSION['lang'] . '">'
    ?>
        <img class="arrows" src="images/arrows/forward-arrows-arrow-front-go_gold.png" onmouseover="this.src='images/arrows/forward-arrows-arrow-front-go_white.png'" onmouseout="this.src='images/arrows/forward-arrows-arrow-front-go_gold.png'">
        </a>
    <?php
    } else {
        echo '<button type="button"></button>';
    }
    ?>
</div>
<?php include('templates/bottom.php'); ?>