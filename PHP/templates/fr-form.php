<!--formulaire commun aux deux types de contact, adaptatif-->

<div class="contenu2">
    <?php if($_GET['title'] == 'recommandations') {
            echo'Vous pouvez poster une recommandation ici.';
        }else{
            echo'Vous pouvez m\'envoyer un message d\'ici.';
        } ?>
        <br>Tous les champs sont requis.
    <form id="form" action="" method="post">
        <!--s'il y a eu des erreurs de saisie, on affiche les données rentrées-->
        <div class="item">
            <label for="name"> Prénom Nom :</label>
            <input type="text" id="name" name="name" value="<?php if (isset($_POST['name'])) {
                                                                echo $_POST['name'];
                                                            } ?>" placeholder=" Prénom Nom" required>
        </div>
        <div class="item">
            <label for="email"> e-mail :</label>
            <input type="email" id="email" name="email" value="<?php if (isset($_POST['email'])) {
                                                                    echo $_POST['email'];
                                                                } ?>" placeholder=" votreemail@xxx.xx" required>
        </div>
        <div class="item">
            <!-- seul élément différent, l'objet pour le mail ou l'entreprise pour la recommandation-->
            <?php
            if ($pageTitle == 'Contact') {
                echo '<label for="subject"> Objet :</label>
        <input type="text" id="subject" name="subject" value="';
                if (isset($_POST['subject'])) {
                    echo $_POST['subject'];
                }
                echo '" placeholder=" objet" required>';
            } else {
                echo '<label for="entreprise"> Entreprise :</label>
            <input type="text" id="entreprise" name="entreprise" value="';
                if (isset($_POST['entreprise'])) {
                    echo $_POST['entreprise'];
                }
                echo '" placeholder=" votre entreprise" required>';
            }
            ?>
        </div>
        <div class="item">
            <label for="msg">Message :</label>
            <textarea id="msg" name="content" rows="4" value="<?php if (isset($_POST['content'])) {
                                                            echo $_POST['content'];
                                                        } ?>" placeholder=" Votre message (500 caractères maximum)" required></textarea>
        </div>
        <div class="item" id="validCGU">
            <input type="checkbox" id="acceptCGU" name="accept" required>
            <label for="acceptCGU">J'accepte les <a href="index.php?page=cgu&title=CGU-RGPD&lang=fr"> CGU</a></label>
        </div>
        <!--élément de sécurité, un token anti-csrf-->
        <?php $csrfProtector->showCsrfTokenField(); ?>
        <?php
        //affiche les éventuelles erreurs de validations
        if (!empty($errors)) {
            echo '<ul class="error">';
            foreach ($errors as $error) {
                foreach ($error as $fieldError) {
                    echo '<li>' . $fieldError . '</li>';
                }
            }
            echo '</ul>';
        } elseif (isset($errors) && $pageTitle == 'Contact') {
            echo '<p class="error">Votre message a bien été envoyé. Merci!</p>';
        }
        ?>

        <!--2ème élément de sécurité, le bouton intégre le captcha invisible-->
        <div class="button">
            <button type="submit" class="g-recaptcha" data-sitekey="6Le0-doUAAAAABDV6zrwbPT62xYcdIHP7EzAmE8W" data-callback='onSubmit'>&nbsp;Envoyer&nbsp;</button>
        </div>
    </form>
</div>