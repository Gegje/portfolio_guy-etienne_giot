<!-- 
    ce fichier ne contient que le menu du site ! 
-->
<nav>
    <div class="conteneur-nav">
        <label for="burger"><img src="images/menu-24px.svg"></label>
        <input type="checkbox" id="burger" role="button">
        <ul>
            <li><a href="index.php?page=default&title=<?php
                                                        if ($_SESSION['lang'] == 'en') {
                                                            echo 'educational_background';
                                                        } else {
                                                            echo 'formations';
                                                        }
                                                        echo '&lang=' . $_SESSION['lang'] . '">';
                                                        if ($_SESSION['lang'] == 'en') {
                                                            echo 'Background';
                                                        } else {
                                                            echo 'Formations';
                                                        } ?> </a></li>
            <li><a href=" index.php?page=default&title=experiences&lang=<?php echo $_SESSION['lang']; ?>">Experiences </a></li>
            <li><a href="index.php?page=default&title=<?php
                                                        if ($_SESSION['lang'] == 'en') {
                                                            echo 'school_projects';
                                                        } else {
                                                            echo 'projets_scolaires';
                                                        }
                                                        echo '&lang=' . $_SESSION['lang'] . '">';
                                                        if ($_SESSION['lang'] == 'en') {
                                                            echo 'School Projects';
                                                        } else {
                                                            echo 'Projets d\'école';
                                                        } ?> </a></li>
            <li><a href="index.php?page=stage&id=0<?php
                                                        echo '&lang=' . $_SESSION['lang'] . '" target="_blank">';
                                                        if ($_SESSION['lang'] == 'en') {
                                                            echo 'Internship at Toolin';
                                                        } else {
                                                            echo 'Stage chez Toolin';
                                                        } ?> </a></li>
            <li><a href=" index.php?page=contacts&title=recommandations&lang=<?php echo $_SESSION['lang']; ?>">Recommandations</a></li>
            <li><a href="index.php?page=contacts&title=contact&lang=<?php echo $_SESSION['lang']; ?>">Contact</a></li>
            <li><a href="index.php?page=cgu&title=<?php
                                                    if ($_SESSION['lang'] == 'en') {
                                                        echo 'GCU-GPDR';
                                                    } else {
                                                        echo 'CGU-RGPD';
                                                    }
                                                    echo '&lang=' . $_SESSION['lang'] . '">';
                                                    if ($_SESSION['lang'] == 'en') {
                                                        echo 'GCU-GPDR';
                                                    } else {
                                                        echo 'CGU-RGPD';
                                                    }
                                                    echo '</a></li>'; ?>
        </ul>
    </div>
</nav>