<!-- include le head et le header... -->
<?php include("templates/top.php") ?>
<div id="home" class="conteneur">
    <?php
    if ($_SESSION['lang'] == 'en') {
        echo '<div class="aboutme"><img src="images/' . $contents[0]['picture'] . '" alt="picture of Guy-Etienne Giot">' . $contents[0]['content'] . '</div>
            <p>When you enter this website, you implicitly accept the <a href="index.php?page=cgu&title=GCU-GPRD&lang=en">GCU</a><p>';
    } else {
        echo '<div class="aboutme"><img src="images/' . $contents[0]['picture'] . '" alt="photo de Guy-Etienne Giot">' . $contents[0]['content'] . '</div>
            <p>En naviguant sur ce site, vous en acceptez implicitement les <a href="index.php?page=cgu&title=CGU-RGPD&lang=fr">CGU</a><p>';
    }
    ?>
</div>
<?php include("templates/bottom.php"); ?>