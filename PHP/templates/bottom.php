<?php
//contient les bas de pages (boutons de navigations, citation et footer)


//affiche la citation pour la page en cours
if (!empty($quote)) {
        echo '<h2 class="citation">' . $quote['content'] . '</h2>';
}

//définit les pages précédente et suivante en fonction de la page en cours
if ($_GET['page'] == 'default' or $_GET['page'] == 'contacts') {
        if ($_GET['title'] == 'formations') {
                $previous = 'index.php?page=home&lang=fr';
                $next = 'index.php?page=default&title=experiences&lang=fr';
        } elseif ($_GET['title'] == 'educational_background') {
                $previous = 'index.php?page=home&lang=en';
                $next = 'index.php?page=default&title=experiences&lang=en';
        } elseif ($_GET['title'] == 'experiences') {
                if ($_SESSION['lang'] == 'fr') {
                        $previous = 'index.php?page=default&title=formations&lang=fr';
                        $next = 'index.php?page=default&title=projets_scolaires&lang=fr';
                } else {
                        $previous = 'index.php?page=default&title=educational_background&lang=en';
                        $next = 'index.php?page=default&title=school_projects&lang=en';
                }
        } elseif ($_GET['title'] == 'projets_scolaires' || $_GET['title'] == 'school_projects') {
                $previous = 'index.php?page=default&title=experiences&lang=' . $_SESSION['lang'] . '';
                $next = 'index.php?page=contacts&title=recommandations&lang=' . $_SESSION['lang'] . '';
        } elseif ($_GET['title'] == 'recommandations') {
                if ($_SESSION['lang'] == 'fr') {
                        $previous = 'index.php?page=default&title=projets_scolaires&lang=fr';
                } else {
                        $previous = 'index.php?page=default&title=school_projects&lang=en';
                }
                $next = 'index.php?page=contacts&title=contact&lang=' . $_SESSION['lang'] . '';
        } elseif ($_GET['title'] == 'contact') {
                $previous = 'index.php?page=contacts&title=recommandations&lang=' . $_SESSION['lang'] . '';
        }
} elseif ($_GET['page'] == 'home') {
        if ($_SESSION['lang'] == 'fr') {
                $next = 'index.php?page=default&title=formations&lang=fr';
        } else {
                $next = 'index.php?page=default&title=educational_background&lang=en';
        }
};

//s'il y a une page précédente
if (isset($previous)) {
?>
        <!--le bouton de gauche y envoie -->
        <a href="<?php echo $previous ?>">
                <div class="button">
                        <img class="boutonG" src="images/arrows/back-arrow_icon-blue.png" onmouseover="this.src='images/arrows/back-arrow_icon-white.png'" onmouseout="this.src='images/arrows/back-arrow_icon-blue.png'">
                </div>
        </a>
<?php
} else {
        echo '<div class="button"></div>';
}


//s'il y a une page suivante
if (isset($next)) {
        //le bouton de droite y envoie
        echo '<a href="' . $next . '">';
        ?>
                <div class="button">
                        <img class="boutonD" src="images/arrows/forward-arrow_blue.png" onmouseover="this.src='images/arrows/forward-arrow_white.png'" onmouseout="this.src='images/arrows/forward-arrow_blue.png'">
                </div>
        </a>
        <?php
} else {
        echo '<div class="button"></div>';
} ?>

</main>
<footer>
        <?php
        echo '<div class="count"><p>page vue ' . $visitsCount['count'] . ' fois depuis ' . $visitsCount['nbIP'] . ' adresse(s) IP.';
        ?>
        </p>
        </div>
        <div>
                <p>&copy; Guy-Etienne Giot 2020</p>
        </div>
</footer>
</body>

</html>