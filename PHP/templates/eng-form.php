<!--formulaire commun aux deux types de contact, adaptatif-->

<div class="contenu2">
    <?php if($_GET['title'] == 'recommandations') {
            echo'You can post a recommandation here.';
        }else{
            echo'You can send me a message from here.';
        } ?>
        <br>All fields are required.

    <form id="form" action="" method="post">
        <!--s'il y a eu des erreurs de saisie, on affiche les données rentrées--> 
        <div class="item">
            <label for="name"> Firstname Lastname :</label>
            <input type="text" id="name" name="name" value="<?php if (isset($_POST['name'])) {
                                                                echo $_POST['name'];
                                                            } ?>" placeholder=" Firstname Lastname" required>
        </div>
        <div class="item">
            <label for="email"> e-mail :</label>
            <input type="email" id="email" name="email" value="<?php if (isset($_POST['email'])) {
                                                                    echo $_POST['email'];
                                                                } ?>" placeholder=" youremail@xxx.xx" required>
        </div>
        <div class="item">
            <!-- seul élément différent, l'objet pour le mail ou l'entreprise pour la recommandation-->
            <?php
            if ($pageTitle == 'Contact') {
                echo '<label for="subject"> Object :</label>
        <input type="text" id="subject" name="subject" value="';
                if (isset($_POST['subject'])) {
                    echo $_POST['subject'];
                }
                echo '" placeholder=" subject" required>';
            } else {
                echo '<label for="entreprise"> Entreprise :</label>
            <input type="text" id="entreprise" name="entreprise" value="';
                if (isset($_POST['entreprise'])) {
                    echo $_POST['entreprise'];
                }
                echo '" placeholder=" your entreprise" required>';
            }
            ?>
        </div>
        <div class="item">
            <label for="msg">Message :</label>
            <textarea id="msg" name="content" rows="4" value="<?php if (isset($_POST['content'])) {
                                                            echo $_POST['content'];
                                                        } ?>" placeholder=" Your message (500 characters maximum)" required></textarea>
        </div>
        <div class="item" id="validCGU">
            <input type="checkbox" id="acceptCGU" name="accept" required>
            <label for="acceptCGU">I accept the <a href="index.php?page=cgu&title=GCU-GPRD&lang=fr"> GCU</a></label>
        </div>
        <!--élément de sécurité, un token anti-csrf-->
        <?php $csrfProtector->showCsrfTokenField(); ?>
        <?php
        //affiche les éventuelles erreurs de validations
        if (!empty($errors)) {
            echo '<ul class="errors">';
            foreach ($errors as $error){
                foreach ($error as $fieldError){
                    echo '<li>'.$fieldError.'</li>';
                }
            }
            echo '</ul>';
        } elseif (isset($errors) && $pageTitle=='Contact'){
            echo '<p class="error">Your message has been sent. Thank you!</p>';
        }
        ?>

        <!--2ème élément de sécurité, le bouton intégre le captcha invisible-->
        <div class="button">
        <button type= "submit" class="g-recaptcha" data-sitekey="6Le0-doUAAAAABDV6zrwbPT62xYcdIHP7EzAmE8W" data-callback='onSubmit'>&nbsp;Send&nbsp;</button>
        </div>
    </form>
</div>