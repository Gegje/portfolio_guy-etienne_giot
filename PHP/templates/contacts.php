<!--page commune pour les recommandations et les messages-->

<!--inclut le haut de page-->
<?php include('templates/top.php'); ?>
<div id="formbox" class="conteneur">
    <?php
    //si c'est la page de recommandations
    if ($pageTitle == 'Recommandations') {
        echo '<div id="recommandations">';
        //affiche les recommandations récupérées en base de données
        if (!empty($recommandations)) {
            foreach ($recommandations as $recommandation) {
                echo '<div class="recommandation" class="contenu">';
                echo '<div class="content">" ' . $recommandation['content'] . ' "</div>';
                echo '<div class="author">de ' . $recommandation['name'] . ', ' . $recommandation['entreprise'] . ', le ' . $recommandation['post_date'] . '</div>';
                echo '</div>';
            }
        }
        echo '</div>';
    } //sinon
    else { ?>
        <!--affiche les coordonnées-->
        <div class="coordonnees">
            <div class="coordonnee" id="title">
                <h2><?php if ($_SESSION['lang'] == 'fr') {
                        echo 'Coordonnées : ';
                    } else {
                        echo 'Contact Information : ';
                    } ?></h2>
            </div>
            <div class="coordonnee">tel : 
                <a href="tel:+33686403789">
                    (+33) 06 86 40 37 89 &nbsp;
                    <button type="button">
                        <?php if ($_SESSION['lang'] == 'fr') {
                                                echo '&nbsp;Appeler&nbsp;';
                                            } else {
                                                echo '&nbsp; Call &nbsp;';
                                            } ?></button>
                </a>
            </div>
            <div class="coordonnee">
                <?php if ($_SESSION['lang'] == 'fr') {
                    echo 'adresse :<br>1 allée Arthur Christiansen<br>44230 SAINT-SEBASTIEN-SUR-LOIRE<br>
                                        <a href="https://www.google.com/maps/place/R%C3%A9sidence+Chrys%C3%A9is/@47.2079292,-1.5018622,17z/data=!4m5!3m4!1s0x4805ef346e317623:0xb8206fcd4f239a1a!8m2!3d47.2076568!4d-1.5015992" target= "_blank">(Localiser avec Google Maps)</a>';
                } else {
                    echo 'address :<br>1 allée Arthur Christiansen<br>44230 SAINT-SEBASTIEN-SUR-LOIRE<br>FRANCE<br>
                                        <a href="https://www.google.com/maps/place/R%C3%A9sidence+Chrys%C3%A9is/@47.2079292,-1.5018622,17z/data=!4m5!3m4!1s0x4805ef346e317623:0xb8206fcd4f239a1a!8m2!3d47.2076568!4d-1.5015992" target= "_blank">(Locate with Google Maps)</a>';
                } ?>
            </div>
            <div class="coordonnee" id="cv">
                <a href="docs/G-E_GIOT.pdf" target="_blank">
                    <div class="button">
                        <button type="button">
                            <?php if ($_SESSION['lang'] == 'fr') {
                                    echo ' Télécharger mon C-V. ';
                                } else {
                                    echo ' Download my C-V.';
                                } ?>
                        </button>
                    </div>
                </a>
            </div>
        </div>
    <?php
    }
    //inclut le formulaire commun aux deux types de messages
    if ($_SESSION['lang'] == 'fr') {
        include('templates/fr-form.php');
    } else {
        include('templates/eng-form.php');
    }

    //inclut le bas de page
    include("templates/bottom.php"); ?>