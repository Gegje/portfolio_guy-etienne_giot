<?php 

class Comment 
{
    //propriétés

    private $id;
    private $authorId;
    private $postId;
    private $content;
    private $dateCreated;

    //getters and setters

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getAuthor_id()
    {
        return $this->author_id;
    }

    public function setAuthor_id($author_id)
    {
        $this->author_id = $author_id;

        return $this;
    }

    public function getPost_id()
    {
        return $this->post_id;
    }

    public function setPost_id($post_id)
    {
        $this->post_id = $post_id;

        return $this;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }
}

?>