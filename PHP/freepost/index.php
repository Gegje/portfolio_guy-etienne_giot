<?php
//on est dans le contrôleur frontal \o/
//ce fichier reçoit toutes les requêtes au site

//on inclue ici toutes nos classes ! 

spl_autoload_register();

//rend disponible toutes les dépendances chargées par composer
include('vendor/autoload.php');

include('templates/DB.php');

session_start();


//on instancie notre contrôleur où sont toutes nos méthodes
//pour chaque page
//on inclue d'abord la définition de la classe
$controller = new Controller();

//si on n'a pas de paramètre dans l'URL... c'est l'accueil
if (empty($_GET['page'])) {
    if (!empty($_GET['searchbykeywords']) OR !empty($_GET['searchbyauthor'])) {
        $controller->search();
    } else {
        $_GET['page'] = 'home';
        $controller->home();
    }
} elseif ($_GET['page'] == 'home'){
    $controller->home();
}
//si on a le paramètre page qui vaut cgu... c'est la page cgu
elseif ($_GET['page'] == 'cgu') {
    $controller->cgu();
} elseif ($_GET['page'] == 'home2'){
    $controller->home2();
} elseif ($_GET['page'] == 'inscription') {
    $controller->register();
} elseif ($_GET['page'] == 'connexion') {
    $controller->login();
} elseif ($_GET['page'] == 'déconnexion') {
    $controller->logout();
} elseif ($_GET['page'] == 'publication') {
    $controller->createPost();
} elseif ($_GET['page'] == 'details') {
    $controller->detail();
} elseif ($_GET['page'] == 'recherche') {
    $controller->search();
} elseif ($_GET['page'] == 'clap'){
    $controller->clap();
} elseif ($_GET['page'] == 'follow'){
    $controller->follow();
} elseif ($_GET['page'] == 'fil'){
    $controller->fil();
} elseif ($_GET['page'] == 'profil'){
    $controller->profil();
}
//si on n'a pas trouvé la page, alors c'est une erreur 404 ! 
else {
    $controller->fourofour();
}
