<?php 

class Post 
{
    //propriétés ou attributs
    //public et private sont des visibilités
    //public : accessible et modifiable de partout
    //private : impossible de modifier ou de lire depuis l'extérieur de la class
    private $id;
    private $title;
    private $content;
    private $picture;
    private $author_id;
    private $createdDate;

    //cette méthode publique permet de liiiiiire la valeur
    //de l'id depuis l'extérieur de la classe
    //ces fonctions quir retournent les valeurs s'appelent des
    //getter
    //en français : accesseur :(
    public function getId()
    {
        return $this->id;
    }

    //cette fonction permet de donner une valeur à l'id ! 
    //ces fonctions qui permettent de modifier les valeurs s'appelent des setter
    //en français : mutateur :(
    public function setId($newId)
    {
        if(!is_numeric($newId)){
            die("hey dude tu fais quoi. Un nombre en id stp.");
        }

        $this->id = $newId;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    public function getPicture()
    {
        return $this->picture;
    }
 
    public function setPicture($picture)
    {
        $this->picture = $picture;

        return $this;
    }

    public function getAuthor_id()
    {
        return $this->author_id;
    }
 
    public function setAuthor_id($author_id)
    {
        $this->author_id = $author_id;

        return $this;
    }

    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    //fonction qui affiche le titre de l'article en HTML
    //les fonctions définies dans les classes s'appelle des
    //méthodes
    public function show()
    {
        //accède au titre de l'instance dans laquelle on se trouve
        //$this fait toujours référence à l'instance actuelle de la classe
        echo '<h2>' . $this->title . '</h2>';
    }
}