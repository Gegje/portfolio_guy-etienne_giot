<?php

class Controller
{
    public function home()
    {
        //requête à la bdd pour aller chercher les derniers articles
        $manager = new Manager();
        $foundPosts = $manager->find12PostsByClaps();
        include("templates/home.php");
    }


    public function home2()
    {
        //requête à la bdd pour aller chercher les derniers articles
        $manager = new Manager();
        $foundPosts = $manager->findLast12Posts();
        include("templates/home2.php");
    }

    public function search()
    {
        $manager = new Manager();
        //lance une recherche par auteur
        if (!empty($_GET['searchbyauthor'])) {
            $query = strip_tags($_GET['searchbyauthor']);
            $foundPosts = $manager->findPostsByAuthor($query);

            //ou par mots-clés   
        } elseif (!empty($_GET['searchbykeywords'])) {
            $query = strip_tags($_GET['searchbykeywords']);
            $foundPosts = $manager->findPostsByKeywords($query);

            //sinon erreur 404
        } else {
            $this->fourofour();
        }

        //récupère la liste des auteurs suivis si l'utilisateur est connecté
        if (!empty($_SESSION['user'])) {
            $followeds = $manager->findFollowedByUser($_SESSION['user']['id']);
        }

        include("templates/search.php");
    }


    public function cgu()
    {
        include("templates/cgu.php");
    }

    public function register()
    {
        //traitement du formulaire
        //formulaire soumis ? 
        if (!empty($_POST)) {
            //récupére les données soumises 
            //strip_tags() pour se protéger des attaques XSS
            $username = strip_tags($_POST['username']);
            $email = strip_tags($_POST['email']);
            $password = $_POST['password'];
            if (!empty($_POST['avatar'])) {
                $avatar = strip_tags($_POST['avatar']);
            } else {
                $avatar = "";
            }
            //valider les données 
            //tableau qui stocke nos éventuels messages d'erreur
            $errors = [];
            $formIsValid = true;
            //validation du username
            if (empty($username)) {
                //on note qu'on a trouvé une erreur ! 
                $formIsValid = false;
                $errors[] = "Veuillez renseigner votre pseudo !";
            } elseif (mb_strlen($username) <= 1) {
                $formIsValid = false;
                $errors[] = "Votre pseudo est court, très court. Veuillez le rallonger !";
            } elseif (mb_strlen($username) > 30) {
                $formIsValid = false;
                $errors[] = "Votre pseudo est trop long !";
            }

            //validation de l'email
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $formIsValid = false;
                $errors[] = "Votre email n'est pas valide !";
            }

            if ($formIsValid == true) {
                //créer une instance de notre modèle User
                $user = new User();

                //hashe le mdp 
                //md5 est ultra-mort
                //sha1 est mort
                //bcrypt  ou  argon2i sont top
                $hash = password_hash($password, PASSWORD_DEFAULT);

                //hydrate l'instance 
                $user->setUsername($username);
                $user->setEmail($email);
                $user->setPassword($hash);
                $user->setAvatar($avatar);

                //demande au Manager de sauvegarder cet user
                $manager = new Manager();
                $userId = $manager->saveNewUser($user);

                //connexion du user !!!! 
                $foundUser = $manager->findUserById($userId);
                $_SESSION['user'] = $foundUser;

                //si l'utilisateur provenait s'est inscrit pour une action précise (post ou comment)
                //on le redirige où il voulait aller, sinon vers l'accueil (404 si id étrange)
                header ('location: index.php?page='.$_SESSION['fromPage'].'');
            }
        }
        include("templates/register.php");
    }


    //afficher et traiter la page de connexion
    public function login()
    {
        //si le form est soumis... 
        if (!empty($_POST)) {
            //par défaut je dis que c'est pas valide
            $formIsValid = false;

            //récupérer le username ou l'email 
            $usernameOrEmail = $_POST['username'];
            //récupérer le mot de passe du form
            $password = $_POST['password'];

            //aller chercher dans la bdd la ligne correspondant à l'email
            $manager = new Manager();
            $foundUser = $manager->findUserByUsernameOrEmail($usernameOrEmail);

            //si on a trouvé une ligne, un user avec cet email ou pseudo...
            if (!empty($foundUser)) {
                //on compare son mdp
                $passwordIsValid = password_verify($password, $foundUser['password']);

                //si le mdp est bon...
                if ($passwordIsValid) {
                    //connexion du user !!!!
                    $_SESSION['user'] = $foundUser;

                    //si l'utilisateur provenait s'est inscrit pour une action précise (post ou comment)
                    //on le redirige où il voulait aller, sinon vers l'accueil (404 si id étrange) 
                    header ('location: index.php?page='.$_SESSION['fromPage'].'');
                }
            }
        }

        include("templates/login.php");
    }


    public function createPost()
    {
        if (!empty($_POST)) {
            $title = strip_tags($_POST['title']);
            $content = strip_tags($_POST['content']);

            //seulement si on a un upload, on valide ! 
            //erreur 4 : veut dire qu'aucun fichier n'a été envoyé
            if ($_FILES['picture']['error'] != 4) {
                //le fichier temporaire, uploadé sur le serveur
                $file = $_FILES['picture']['tmp_name'];

                //on s'assure que le type du fichier est safe
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $mime = finfo_file($finfo, $file);

                //les types mime que j'accepte
                $acceptedTypes = ["image/jpeg", "image/jpg", "image/png"];

                //je cherche le mime du fichier parmi ceux que j'accepte
                if (!in_array($mime, $acceptedTypes)) {
                    $errors[] = "Type de fichier non accepté !";
                }

                // renseigner correctement les tailles dans le php.ini !!!
                // post_max_size 
                // upload_max_filesize 
                $size = $_FILES['picture']['size'];
                if ($size > 20000000) {
                    $errors[] = "Fichier trrop gros. 20 mb max svp.";
                }



                //on devine l'extension du fichier
                $extension = str_replace("image/", ".", $_FILES['picture']['type']);

                //renomme l'image
                //génère une chaîne toujours unique
                $picture = uniqid() . $extension;

                //si l'upload est valide
                if (empty($errors)) {
                    //déplace le fichier temporaire vers mon dossier à moi
                    move_uploaded_file(
                        $_FILES['picture']['tmp_name'],
                        "img/uploads/$picture"
                    );

                    //on utilise SimpleImage pour redimensionner notre image
                    //voir https://github.com/claviska/SimpleImage 
                    $simpleImage = new \claviska\SimpleImage();

                    $simpleImage
                        //on redimensionne ce fichier
                        ->fromFile("img/uploads/$picture")
                        //en max 700 en 700
                        ->bestFit(700, 700)
                        //écrase l'image
                        ->toFile("img/uploads/$picture", null, 80);
                }
            }

            //validation des valeurs
            if (empty($title)) {
                $errors[] = "Le titre est requis !";
            } elseif (strlen($title) > 191) {
                $errors[] = "191 caractères max pour le titre svp !";
            }
            if (empty($content)) {
                $errors[] = "Le contenu de l'article est requis !";
            } elseif (strlen($content) > 10000) {
                $errors[] = "10000 caractères max pour le contenu svp !";
            }

            //si tout est validé, on crée le nouveau post
            if (empty($errors)) {
                $post = new Post();

                $post->setTitle($title);
                $post->setContent($content);
                $post->setPicture($picture);

                $manager = new Manager();
                $postId = $manager->saveNewPost($post);
                //redirige vers la page de détails du nouveau post
                header("location: index.php?page=details&id=$postId");
            }
        }
        //tout ceci n'est possible que si l'utilisateur est connecté
            include('templates/create_post.php');
    }

    public function detail()
    {
        $postId = strip_tags($_GET['id']);

        //récupére les données du post à afficher
        $manager = new Manager();
        $details = $manager->findPostById($postId);

        //crée un commentaire si déposé
        if (!empty($_POST['commentaire'])) {
            $author_id = $_SESSION['user']['id'];
            $content = strip_tags($_POST['commentaire']);

            $comment = new Comment();
            $comment->setAuthor_id($author_id);
            $comment->setPost_id($postId);
            $comment->setContent($content);

            $manager->saveNewComment($comment);
            header("location: index.php?page=details&id=$postId");
        }

        //récupère les commentaires à afficher
        $comments = $manager->getCommentsByPostId($postId);

        //récupère la liste des auteurs suivis si l'utilisateur est connecté
        if (!empty($_SESSION['user'])) {
            $followeds = $manager->findFollowedByUser($_SESSION['user']['id']);
        }

        include("templates/details.php");
    }

    public function clap()
    {
        $postId = $_GET['id'];

        $manager = new Manager();
        //on incrémente le claps_count de l'article
        $manager->incrementClapsCount($postId);

        //on redirige vers la page précédente
        header('Location: index.php?page=details&id=' . $postId);
    }

    public function follow()
    {
        $user1Id = $_SESSION['user']['id'];
        $query = $_GET['name'];

        $manager = new Manager();
        //on commence par récupérer l'id de l'auteur qu'on veut suivre
        //en utilisant une méthode déjà créée pour le login
        $foundUser = $manager->findUserByUsernameOrEmail($query);

        //on crée une ligne dans la table de relation
        $manager->createFollowing($user1Id, $foundUser['id']);

        //on récupère les articles de l'auteur auquel on vient de s'abonner
        $foundPosts = $manager->findPostsByAuthor($query);

        include("templates/follow.php");
    }


    public function fil()
    {
        $manager = new Manager();
        $foundPosts = $manager->findPostsByFollowed($_SESSION['user']['id']);

        include("templates/fil.php");
    }


    public function profil()
    {
        $manager = new Manager();
        $followeds = $manager->findFollowedByUser($_SESSION['user']['id']);
        $followers = $manager->findFollowersByUser($_SESSION['user']['id']);
        $foundPosts = $manager->findPostsByAuthor($_SESSION['user']['username']);

        include("templates/profil.php");
    }

    public function logout()
    {
        //supprimer les données de sessions
        unset($_SESSION['user']);

        //ou plus brutalement
        //session_destroy();
        header("Location: index.php");
        die();
    }

    public function fourofour()
    {
        include("templates/404.php");
        die();
    }
}
