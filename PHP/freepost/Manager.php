<?php

/**
 * Cette classe contient toutes les requêtes SQL !
 * Une requête par méthode ! 
 * SRP : single responsability principle
 */
class Manager
{
    //crée une nouvel utilisateur en bdd 
    public function saveNewUser($user)
    {
        $sql = "INSERT INTO 
                users (email, username, password, avatar, created_date) 
                VALUES 
                (:email, :username, :password, :avatar, NOW())";

        //on utilise la classe DbConnection pour récupérer 
        //notre connexion à la base de données ($pdo)

        $pdo = DbConnection::getPdo();

        //envoie la requête au serveur sql
        $stmt = $pdo->prepare($sql);

        //exécute la requête SQL avec les données du user
        //on appelle tous les getters de notre classe pour récupérer les données
        $stmt->execute([
            ":email" => $user->getEmail(),
            ":username" => $user->getUsername(),
            ":password" => $user->getPassword(),
            ":avatar" => $user->getAvatar(),
        ]);

        $userId = $pdo->lastInsertId();
        return $userId;
    }

    //récupère une ligne depuis la table users
    //en fonction d'un username ou d'un email
    public function findUserById($userId)
    {
        $sql = "SELECT id, username, avatar FROM users 
                WHERE id = :userId";

        //récupère notre connexion à la bdd
        $pdo = DbConnection::getPdo();

        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            ":userId" => $userId,
        ]);

        $foundUser = $stmt->fetch();
        return $foundUser;
    }


    //récupère une ligne depuis la table users
    //en fonction d'un username ou d'un email
    public function findUserByUsernameOrEmail($usernameOrEmail)
    {
        $sql = "SELECT * FROM users 
                WHERE username = :username 
                OR email = :email";

        //récupère notre connexion à la bdd
        $pdo = DbConnection::getPdo();

        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            ":username" => $usernameOrEmail,
            ":email" => $usernameOrEmail,
        ]);

        $foundUser = $stmt->fetch();
        return $foundUser;
    }


    //retrouve les auteurs auquel un user est abonné
    public function findFollowedByUser($userId)
    {
        $sql = "SELECT username, avatar FROM
        (SELECT user2_id FROM follows WHERE user1_id=:userId) as followed
        JOIN users ON users.id = followed.user2_id";

        $pdo = DbConnection::getPdo();
        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            ":userId" => $userId,
        ]);
        $followeds = $stmt->fetchAll();
        return $followeds;
    }


    //retrouve les membres qui suivent l'utilisateur
    public function findFollowersByUser($userId)
    {
        $sql = "SELECT username, avatar FROM
        (SELECT user1_id FROM follows WHERE user2_id=:userId) as follower
        JOIN users ON users.id = follower.user1_id";

        $pdo = DbConnection::getPdo();
        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            ":userId" => $userId,
        ]);
        $followeds = $stmt->fetchAll();
        return $followeds;
    }


    //récupère les articles d'un auteur quand l'utilisateur s'y abonne
    public function findPostsByFollowed($userId)
    {
        $sql = "SELECT * FROM
                (SELECT user2_id FROM follows WHERE user1_id=:userId) as followed
                JOIN users ON users.id = followed.user2_id
                JOIN posts ON posts.author_id=users.id
                ORDER BY date_created DESC
                LIMIT 12";
        $pdo = DbConnection::getPdo();
        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            ":userId" => $userId,
        ]);
        $foundUsers = $stmt->fetchAll();
        return $foundUsers;
    }



    //enregistre un post en bdd
    public function saveNewPost($post)
    {
        $author_id = $_SESSION['user']['id'];
        $sql = "INSERT INTO
                posts (title, content, picture, author_id, date_created)
                VALUES (:title, :content, :picture, $author_id, NOW())";

        $pdo = DbConnection::getPdo();

        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            ":title" => $post->getTitle(),
            ":content" => $post->getContent(),
            ":picture" => $post->getPicture(),
        ]);
        $postId = $pdo->lastInsertId();
        return $postId;
    }


    //récupère les 12  articles les plus clappés (pour la page d'accueil)
    public function find12PostsByClaps()
    {
        $pdo = DbConnection::getPdo();

        $sql = "SELECT posts.id as id, posts.title as title, posts.picture as picture, posts.date_created as date_created, posts.claps_count as claps_count, users.username as username
                FROM posts JOIN users ON posts.author_id = users.id
                ORDER BY claps_count DESC
                LIMIT 12";

        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        $posts = $stmt->fetchALL();
        return $posts;
    }


    //récupère les 12 derniers articles publiés (pour la page d'accueil)
    public function findLast12Posts()
    {
        $pdo = DbConnection::getPdo();

        $sql = "SELECT posts.id as id, posts.title as title, posts.picture as picture, posts.date_created as date_created, posts.claps_count as claps_count, users.username as username
                FROM posts JOIN users ON posts.author_id = users.id
                ORDER BY date_created DESC
                LIMIT 12";

        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        $posts = $stmt->fetchALL();
        return $posts;
    }


    //récupère tous les articles d'un auteur à partir de son pseudo ou de son email
    public function findPostsByAuthor($query)
    {
        $pdo = DbConnection::getPdo();

        $sql = "SELECT * FROM 
                (SELECT id, username FROM users WHERE username = '$query' or email = '$query') AS user 
                JOIN posts ON posts.author_id = user.id ";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        $foundPosts = $stmt->fetchAll();
        return $foundPosts;
    }


    //fait une recherche par mots clés
    public function findPostsByKeywords($query)
    {
        $pdo = DbConnection::getPdo();

        $search = '%' . str_replace(" ", "%", $query) . "%";

        $sql = "SELECT posts.id, posts.title, posts.content, posts.picture, posts.author_id, posts.claps_count, posts.date_created, users.username
        FROM posts JOIN users on posts.author_id=users.id HAVING posts.content LIKE :search OR posts.title LIKE :search OR users.username LIKE :search";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            ":search" => $search
        ]);

        $foundPosts = $stmt->fetchAll();
        return $foundPosts;
    }


    //retrouve un post en fonction de son id
    public function findPostById($postId)
    {
        $pdo = DbConnection::getPdo();

        $sql = "SELECT posts.id, posts.title, posts.content, posts.picture, posts.author_id, posts.claps_count, posts.date_created, users.username
        FROM posts JOIN users ON posts.author_id = users.id HAVING posts.id = :postId";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            ":postId" => $postId
        ]);

        $details = $stmt->fetch();
        return $details;
    }


    //incrémente le claps_count d'un aricle
    public function incrementClapsCount($postId)
    {
        $pdo = DbConnection::getPdo();

        $sql = "UPDATE posts SET claps_count = claps_count+1 WHERE id=$postId";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
    }


    //crée un abonnement de l'utilisateur à un auteur
    public function createFollowing($user1Id, $user2Id)
    {
        $pdo = DbConnection::getPdo();

        $sql = "INSERT INTO follows (user1_id, user2_id)
                VALUES (:user1Id, :user2Id)";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            ":user1Id" => $user1Id,
            ":user2Id" => $user2Id,
        ]);
        return $user2Id;
    }


    //enregistre un commentaire quand il est déposé
    public function saveNewComment($comment)
    {
        $pdo = DbConnection::getPdo();

        $sql = "INSERT INTO comments (author_id, post_id, content, date_created)
                VALUES (:author_id, :post_id, :content, NOW()) ";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            ":author_id" => $comment->getAuthor_id(),
            ":post_id" => $comment->getPost_id(),
            ":content" => $comment->getContent(),

        ]);
    }


    //récupère les commentaires liés à un article
    public function getCommentsByPostId($postId)
    {

        $pdo = DbConnection::getPdo();

        $sql = "SELECT comments.post_id, comments.content, comments.date_created, users.username
                FROM comments JOIN users ON comments.author_id = users.id 
                HAVING comments.post_id = $postId";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        $comments = $stmt->fetchAll();
        return $comments;
    }
}
