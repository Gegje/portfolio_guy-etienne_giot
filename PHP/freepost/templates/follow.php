<!-- include le head et le header... -->
<?php include("top.php") ?>

<!-- contenu spécifique à cette page -->
<div class="titre">
    <h1>Article(s) publié(s) par <?php echo $query ?></h1>';
</div>
<div class="gallery">
    <?php
    //affiche la liste des articles de l'auteur l'utilisateur vient de s'abonner
    foreach ($foundPosts as $foundPost) {
        $timestamp = strtotime($foundPost['date_created']);
        $dateFr = date("d/m/Y H:i", $timestamp);
        $dateHeure = explode(" ", $dateFr);
        echo '<div class= "minipost"><p> ' . $foundPost['title'] . '</p><img class="miniature" src="img/uploads/' . $foundPost['picture'] . '"><br> posté le ' . $dateHeure[0] . ' à ' . $dateHeure[1] . '<br>
        par ' . $foundPost['username'].'<br>
        <a href ="index.php?page=details&id=' . $foundPost["id"] . '"><button class="btn btn-primary">Voir l\'article</button></a></div>';
    }?>
</div>
    
<!-- inclue le footer et les fermetures de balises -->
<?php include("bottom.php") ?>