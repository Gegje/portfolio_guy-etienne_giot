<!-- include le head et le header... -->
<?php include("top.php");?>

<!-- contenu spécifique à cette page -->
<h2>Connectez-vous !</h2>

<!--on crée le formulaire de login -->
<form method="post">
    <div class="form-group"> 
        <label for="username">Votre pseudo ou email</label>
        <input class="form-control" type="text" name="username" id="username" required>
    </div>
    
    <div class="form-group"> 
        <label for="password">Votre mot de passe</label>
        <input class="form-control" type="password" name="password" id="password" required>
    </div>
   
    <?php 
    //éventuel message d'erreur
    if(isset($formIsValid) && $formIsValid === false){
        echo '<div class="alert alert-danger">Mauvais identifiants</div>';
    }
    ?>
    
    <button class="btn btn-primary">C' est parti !</button>
</form>

<!--si l'utilisateur n'est pas encore inscrit, on lui propose de le faire-->
<p style="margin-left: 5vw">Pas encore de compte?&nbsp;<a href="index.php?page=inscription&id=<?php echo $_GET['id'] ?>"><button class="btn btn-primary">Je m'inscris</button></a></p>
<!-- inclue le footer et les fermetures de balises -->
<?php include("bottom.php") ?>