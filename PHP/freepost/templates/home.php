<!-- include le head et le header... -->
<?php include("top.php");
$_SESSION['fromPage'] = $_GET['page'] ?>

<!-- contenu spécifique à cette page -->
<div class="titre">
    <h1>Bienvenue sur FreePost</h1>
    <h2>Voici les 12 articles les plus plébiscités</h2>
    <h3><a href="index.php?page=home2">Voir les 12 derniers articles</a></h3>
</div>
<div class="gallery">
    <?php
    //affiche une présentation des 12 derniers articles publiés
    foreach ($foundPosts as $foundPost) {
        $timestamp = strtotime($foundPost['date_created']);
        $dateFr = date("d/m/Y H:i", $timestamp);
        $dateHeure = explode(" ", $dateFr);
        echo '<div class= "minipost"><p> ' . $foundPost['title'] . '</p><a href ="index.php?page=details&id=' . $foundPost["id"] . '"><img class="miniature" src="img/uploads/' . $foundPost['picture'] . '"><br>
         posté le ' . $dateHeure[0] . ' à ' . $dateHeure[1] . ' &nbsp; &nbsp; 👏'.$foundPost['claps_count'].'<br>
        par '.$foundPost['username'].'<br>
        <a href ="index.php?page=details&id=' . $foundPost["id"] . '"><button class="btn btn-primary">Voir l\'article</button></a></div>';
    }
    ?>
</div>
<!-- inclue le footer et les fermetures de balises -->
<?php include("bottom.php") ?>