<?php 
include("top.php");

if (empty($_SESSION['user'])){
    $_SESSION['fromPage'] = $_GET['page'];
    header ('location: index.php?page=connexion');
} ?>


<h2>Créez un article !</h2>

<!--on crée le formulaire de publication -->
<form method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="title">Titre de votre article</label>
        <input class="form-control" type="text" name="title" id="title" placeholder="Titre">
    </div>
    <div class="form-group">
        <label for="content">Rédigez votre article</label>
        <textarea class="form-control" name="content" id="content" placeholder="Votre article"></textarea>
    </div>
    <div class="form-group">
        <label for="picture"> Choisissez l'image à associer</label>
        <input type="file" name="picture" id="picture">
    </div>
    <?php
    //affiche les éventuelles erreurs de validations
    if (!empty($errors)) {
        echo '<div class="alert alert-danger">';
        foreach ($errors as $error) {
            echo '<div>' . $error . '</div>';
        }
        echo '</div>';
    }
    ?>

    <button class="btn btn-primary">Envoyer !</button>
</form>
<?php include("bottom.php") ?>