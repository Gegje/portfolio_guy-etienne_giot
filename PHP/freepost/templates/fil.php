<!-- include le head et le header... -->
<?php include("top.php") ?>
<?php

//si l'utilisateur est abonné à au moins un auteur
if (!empty($foundPosts)) {
?>
    <div class="titre">
        <h1>Votre fil d'abonnements</h1>
    </div>
    <div class="gallery">
    <?php
    //affiche les articles des auteurs auxquels l'utilisateur est abonné
    foreach ($foundPosts as $foundPost) {
        $timestamp = strtotime($foundPost['date_created']);
        $dateFr = date("d/m/Y H:i", $timestamp);
        $dateHeure = explode(" ", $dateFr);
        echo '<div class= "minipost"><p> ' . $foundPost['title'] . '</p><a href ="index.php?page=details&id=' . $foundPost["id"] . '"><img class="miniature" src="img/uploads/' . $foundPost['picture'] . '"><br> posté le ' . $dateHeure[0] . ' à ' . $dateHeure[1] . '<br>
        par ' . $foundPost['username'] . '<br>
        <a href ="index.php?page=details&id=' . $foundPost["id"] . '"><button class="btn btn-primary">Voir l\'article</button></a></div>';
    }
} else {
    echo '<h1>Vous ne suivez aucun auteur pour l\'instant!</h1><h2>Allez voir <a href="index.php?page=home2">les derniers articles </a>sur la page d\'accueil</h2>';
}
    ?>
    </div>
    <!-- inclue le footer et les fermetures de balises -->
    <?php include("bottom.php") ?>