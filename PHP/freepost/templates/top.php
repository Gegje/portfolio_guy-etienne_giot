<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Freepost - <?php
            if (isset($_GET['page'])) {
                echo $_GET['page'];
            } else {
                echo "Accueil";
            }
            ?></title>
    <!-- on charge d'abord bootstrap, puis notre css à nous -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/app.css">
</head>

<body>
    <header>
        <h1 class="mr-3"><a href="index.php">FreePost</a></h1>
        <!--si l'utilisateur n'est pas connecté, on affiche des liens de connexion ou inscription
        //s'il est connecté, on affiche un lien de déconnexion, un vers son "fil" et une personnalisation-->
        <nav class="nav">
            <a href="index.php?page=home" class="nav-link">Accueil</a>
            <a href="index.php?page=publication" class="nav-link">Créez un article</a>
            <?php
            
            if (!empty($_SESSION['user'])) {
                echo '<a href="index.php?page=fil" class="nav-link">Votre fil</a>';
                echo '<a href="index.php?page=déconnexion" class="nav-link">Déconnexion</a>';
            } else {
                echo '<a href="index.php?page=inscription" class="nav-link">Inscription</a>';
                echo '<a href="index.php?page=connexion" class="nav-link">Connexion</a>';
            }
            ?>
        </nav>
        <div class="user">
        <?php
        if (!empty($_SESSION['user'])) {
            echo "Bonjour, <a href='index.php?page=profil&id=". $_SESSION['user']['username'] ."'>" . $_SESSION['user']['username'] . "&nbsp <img class='avatar' src='img/avatars/avatar-" . $_SESSION['user']['avatar'] . ".png'></a>";
        }
        ?>
        </div>
        <!--une barre de recherche...-->
        <div class="search">
            <form method="get">
                <input type="text" name="searchbykeywords" id="searchbykeywords" placeholder="🔍 Recherche par mot(s) clé(s) 🔎">
                <button class="btn btn-primary" type="submit">&nbsp; Rechercher &nbsp;</button>
            </form>
        </div>
        </div>

    </header>
    <main>