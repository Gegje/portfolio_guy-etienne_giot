<!-- include le head et le header... -->
<?php include("top.php");
//affiche les données de l'utilisateur (pseudo, email, avatar s'il existe, date de création du compte)
if (!empty($_SESSION['user']['avatar'])){echo '<img src="img/avatars/avatar-'.$_SESSION['user']['avatar'].'.png">';}
echo '<h1>' . $_SESSION['user']['username'].'</h1>';
echo '<br><h2>Votre email : ' . $_SESSION['user']['email'].'</h2>';

$timestamp = strtotime($_SESSION['user']['created_date']);
$dateFr = date("d/m/Y", $timestamp);
echo '<br><h2>Vous êtes membre depuis le '.$dateFr.'<br>';

//la liste des auteurs suivis
if (!empty($followeds)){
echo '<br><h2 style="text-decoration: underline solid black">Les auteurs que vous suivez :</h2>
        <div class="followeds">';
        foreach($followeds as $followed){
            echo '<div class="followed"><img class="avatar" src="img/avatars/avatar-'.$followed['avatar'].'.png">
                    <a href="index.php?searchbyauthor='.$followed['username'].'"> '.$followed['username'].'</a></div>';
        };
        echo '</div>';
}

//liste des membres qui suivent l'utilisateur
if (!empty($followers)){
        echo '<br><h2 style="text-decoration: underline solid black">Les membres qui vous suivent :</h2>
                <div class="followeds">';
                foreach($followers as $follower){
                    echo '<div class="followed"><img class="avatar" src="img/avatars/avatar-'.$follower['avatar'].'.png">
                            <a href="index.php?searchbyauthor='.$follower['username'].'"> '.$follower['username'].'</a></div>';
                };
                echo '</div>';
        }

//récapitulatif des articles publiés par l'utilisateur
if (!empty($foundPosts)){
echo '<h2 style="text-decoration: underline solid black">Les articles que vous avez publiés :</h2>
    <div class="gallery">';
    foreach ($foundPosts as $foundPost) {
        $timestamp = strtotime($foundPost['date_created']);
        $dateFr = date("d/m/Y H:i", $timestamp);
        $dateHeure = explode(" ", $dateFr);
        echo '<div class= "minipost"><p> ' . $foundPost['title'] . '</p><img class="miniature" src="img/uploads/' . $foundPost['picture'] . '"><br> posté le ' . $dateHeure[0] . ' à ' . $dateHeure[1] . '<br>
        <a href ="index.php?page=details&id=' . $foundPost["id"] . '"><button class="btn btn-primary">Voir l\'article</button></a></div>';
}

echo'</div>';
}



?>
<!-- inclue le footer et les fermetures de balises -->
<?php include("bottom.php") ?>