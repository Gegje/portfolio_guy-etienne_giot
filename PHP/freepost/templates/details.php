<?php include("top.php");
//enregistre dans la session la page
$_SESSION['fromPage'] = $_GET['page'].'&id='.$_GET['id'];

$follow = 0;
//vérifie si l'utilisateur est abonné à l'auteur de l'article qu'on va afficher en détails
if (!empty($_SESSION['user'])){
foreach ($followeds as $followed) {
    if ($followed['username'] == $details['username']) {
        $follow = 1;
        break;
    }
}
} ?>
<!--on affiche toutes les données de l'article-->
<div class="details">
    <h1><?php echo $details['title']; ?></h1>
    <p> <?php echo '<a href="index.php?page=clap&id=' . $_GET['id'] . '">👏</a>' . $details['claps_count']; ?></p>
    <div class="post">
        <img src="img/uploads/<?php echo $details['picture']; ?>" alt="<?php echo $details['title']; ?>">
        <?php echo $details['content']; ?>
    </div>

    <div class="author">
        <?php echo 'Posté par : <a href="index.php?searchbyauthor=' . $details['username'] . '">' . $details['username'] . '</a>';
        if (!empty($_SESSION['user'])) {
            if ($details['username'] != $_SESSION['user']['username'] and $follow == 0) {
                echo '  (<a href ="index.php?page=follow&name=' . $details['username'] . '">suivre</a>)';
            }
        } ?>
    </div>
    <div class="date">
        <?php
        $timestamp = strtotime($details['date_created']);
        $dateFr = date("d/m/Y H:i", $timestamp);
        $dateHeure = explode(" ", $dateFr);
        echo "le " . $dateHeure[0] . " à " . $dateHeure[1]; ?>
    </div>
</div>

<!--affichage des commentaires-->
<div class="comments">
    <?php
    foreach ($comments as $comment) {
        $timestamp = strtotime($comment['date_created']);
        $dateFr = date("d/m/Y H:i", $timestamp);
        $dateHeure = explode(" ", $dateFr);
        echo '<div class="comment">' . $comment['content'] . '</div>';
        echo '<p>' . $comment['username'] . ', le ' . $dateHeure[0] . ' à ' . $dateHeure[1] . '</p>';
    }

    //si l'utilisateur est connecté, on lui propose de laisser un commentaire
    if (!empty($_SESSION['user'])) {
    ?>
        <!--via un formulaire-->
        <form method="POST">
            <div class="form-group">
                <label for="commentaire">Déposez un commentaire</label><br>
                <textarea id="commentaire" name="commentaire" placeholder="Votre commentaire"></textarea><br>
                <button class="btn btn-primary">Envoyer !</button>
            </div>
        </form>
    <?php
    } else {
    ?>
    <!--s'il n'est pas connecté, on lui propose de le faire (ou de s'inscrire)-->
        <br><a href="index.php?page=connexion"><button class="btn btn-primary">Connectez vous pour déposer un commentaire!</button></a>
    <?php

    }

    ?>
</div>

<?php include("bottom.php"); ?>