<!-- include le head et le header... -->
<?php include("top.php") ?>

<!-- contenu spécifique à cette page -->
<?php 
if (!empty($foundPosts)){

?>
<div class="titre">
    <?php 
    if  (count($foundPosts)==1){
        echo '<h1>Résultat de votre recherche</h1>';
    } else {
        echo '<h1>Résultats de votre recherche</h1>';
    }
    ?>
    <h2>pour <?php echo $query ?></h2>
</div>
<div class="gallery">
    <?php
    //on affiche tous les posts liés à la recherche
    foreach ($foundPosts as $foundPost) { 
        $follow=0;
        if (!empty($_SESSION['user'])){
        //vérifie si l'utilisateur est abonné aux auteurs pour ne pas lui reproposer de s'abonner
        foreach($followeds as $followed){
            if ($followed['username'] == $foundPost['username']){
                $follow=1;
                break;
            }
        }
        }

        $timestamp = strtotime($foundPost['date_created']);
        $dateFr = date("d/m/Y H:i", $timestamp);
        $dateHeure = explode(" ", $dateFr);
        echo '<div class= "minipost"><p> ' . $foundPost['title'] . '</p><img class="miniature" src="img/uploads/' . $foundPost['picture'] . '"><br> posté le ' . $dateHeure[0] . ' à ' . $dateHeure[1] . '<br>
        par '.$foundPost['username'].'&nbsp;';
        if (!empty($_SESSION['user'])) {
            if ($foundPost['username'] != $_SESSION['user']['username'] and $follow == 0) {
                echo '(<a href ="index.php?page=follow&name=' . $foundPost['username'] . '">suivre</a>)';
            }
        }
        echo'<br><a href ="index.php?page=details&id=' . $foundPost["id"] . '"><button class="btn btn-primary">Voir l\'article</button></a></div>';
    }
 echo'</div>';
} else {
    echo '<h2>Désolé, votre recherche n\'a donné aucun résultat...</h2>';
};

?>
<!-- inclue le footer et les fermetures de balises -->
<?php include("bottom.php") ?>