<?php

class Contact
{
    private $id;
    private $name;
    private $email;

    //à l'instanciation d'un nouveau contact, on lui affecte directement les valeurs name et email
    public function __construct($name, $email)
    {
        $this->setName($name);
        $this->setEmail($email);
    }

    //definit les getters et setters

    public function getId()
    {
        return $this->id;
    }


    public function getName()
    {
        return $this->name;
    }

    private function setName($name)
    {
        $this->name = $name;

        return $this;
    }


    public function getEmail()
    {
        return $this->email;
    }

    private function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }
}
