<?php 
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class Mailer 
{
    const API_KEY = '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';

    private function getBaseEmail($email, $name)
    {
        // Instantiation and passing `true` enables exceptions
        $mail = new PHPMailer(true);
        //Server settings
        //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
        $mail->isSMTP();                                            // Send using SMTP            
        $mail->Host       = 'smtp.sendgrid.net';                    // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = 'apikey';                               // SMTP username
        $mail->Password   = self::API_KEY;                          // SMTP password (key fournie par sentgrid)
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // chiffrement du mail
        $mail->Port       = 587;                                    // port du serveur SMTP
         
        $mail->CharSet = 'UTF-8';

        //expéditeur
        $mail->setFrom($email, $name);
        return $mail;
    }

    public function sendNewMail($name, $email, $subject, $content, $formValidation)
    {
        try {
            //préparation du mail
            $mail = $this->getBaseEmail($email, $name);
            //ajout du destinataire
            $mail->addAddress('guy-etienne.giot@students.campus.academy', 'Guy-Etienne Giot');

            // Contenu du mail (en html)
            $mail->isHTML(true);               
            $mail->Subject = $subject;
            $mail->Body    = $content;

            //envoi du mail
            $mail->send();
            //envoi d'une réponse automatique
            $mail=$this->autoReply($name, $email, $subject, $content);

        } catch (Exception $e) {
            //remplit le tableau d'erreur le cas échéant
            $formValidation -> validateEmailSent($mail->ErrorInfo);
        }
    }

    private function autoReply($name, $email, $subject, $content)
    {
        //préparation de l'email
        $mail = $this->getBaseEmail('guy-etienne.giot@students.campus.academy', 'Guy-Etienne Giot');
        //ajout du destinataire
        $mail->addAddress($email, $name);

        // Contenu du mail (en html)
        $mail->isHTML(true);               
        $mail->Subject = "auto reply : ".$subject;
        $mail->Body    = "Je vous remercie pour votre message.<br>J'en prendrai connaissance dès que possible et vous répondrai, le cas échéant, aussitôt.<br>
                            Pour rappel, votre message était : <br>".$content;

         //envoi du mail
         $mail->send();                   
    }

    public function sendRecommandationForValidation($name, $email, $content)
    {
        $mail = $this->getBaseEmail($email, $name);
            //ajout du destinataire
            $mail->addAddress('guy-etienne.giot@students.campus.academy', 'Guy-Etienne Giot');

            // Contenu du mail (en html)
            $mail->isHTML(true);               
            $mail->Subject = 'nouvelle recommandation de '.$name;
            $mail->Body    = $content;

            //envoi du mail
            $mail->send();
    }
}