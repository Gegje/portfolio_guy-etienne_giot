<?php

//une classe "abstract" ne peut pas être instanciée
abstract class Validator
{
    private $errors;

    //initialise le tableau d'erreurs
    public function __construct()
    {
        $this->errors = [];
    }

    //retourne une booléen, true si le champ comporte au moins une erreur
    public function hasErrors($fieldName)
    {
        if (!empty($this->errors[$fieldName])) {
            return true;
        }

        return false;
    }

    //retourne le tableau d'erreurs
    public function getErrors()
    {
            return $this->errors;
    }

    //retourne un booléen, true si le formulaire est valide
    public function formIsValid()
    {
        $formIsValid = true;
        if (count($this->errors) > 0) {
            $formIsValid = false;
        }

        return $formIsValid;
    }

    //ajoute au tableau d'erreurs celles éventuellement trouvées dans un champ spécifique
    protected function addError($fieldName, $message)
    {
        //les erreurs sont classées par nom de champ dans le tableau
        $this->errors[$fieldName][] = $message;
    }

    //enlève les balises HTML et les espaces au début et à la fin, notamment pour éviter les attaques XSS
    public static function purifyString($string)
    {
        $purified = strip_tags(trim($string));
        return $purified;
    }
}
