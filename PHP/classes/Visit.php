<?php
class Visit
{
    private $id;
    private $page;
    private $visitorIP;
    private $visitorUserAgent;
    private $dateTime;

    //permet de créer automatiquement une visite
    public function __construct($pageName)
    {
        $this->setPage($pageName);
        $this->setVisitorIP();
        $this->setVisitorUserAgent();
        $this->setDatetime();
        
        //envoi pour enregistrement en BDD
        $manager = new Manager;
        $manager->saveNewVisit($this);
    }

    public function getId()
    {
        return $this->id;
    }

    private function setPage($pageName)
    {
        $this->page=$pageName;
    
    }
    public function getPage()
    {
        return $this->page;
    }


    private function setVisitorIP()
    {
        $this->visitorIP=$_SERVER['REMOTE_ADDR'];
    }
    public function getVisitorIP()
    {
        return $this->visitorIP;
    }


    private function setVisitorUserAgent()
    {
        $this->visitorUserAgent=$_SERVER['HTTP_USER_AGENT'];
    }
    public function getVisitorUserAgent()
    {
        return $this->visitorUserAgent;
    }


    private function setDateTime()
    {
        //rècupère la date et l'heure courante
        $now = new DateTime();
        $this->dateTime=$now->format('d-m-Y H:i:s');
    }
    public function getDateTime()
    {
        return $this->dateTime;
    }

}