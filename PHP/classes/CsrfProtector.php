<?php

class CsrfProtector
{
	public function __construct()
	{
		//si la session n'a pas déjà été démarrée, on la démarre
		if (session_status() !== PHP_SESSION_ACTIVE){
			session_start();
		}
	}
	//affiche le champ de formulaire contenant le token
	public function showCsrfTokenField()
	{
		$newToken = $this->generateNewToken();
		echo '<input name="token" type="hidden" value="' . $newToken . '" >';
	}

	//s'assure que le token reçu depuis le formulaire est valide
	//retourne true si valide, false sinon
	public function validateReceivedToken($postToken)
	{
		if ($postToken === $this->getTokenFromSession()){
			return true;
		}
		return false;
		$this -> deleteTokenInSession();
	}

	//génère un nouveau token et le retourne
	private function generateNewToken()
	{
		$newToken = bin2hex(random_bytes(16));
		$this->saveTokenInSession($newToken);
		return $newToken;
	}

	//récupère le token stocké dans la session et le retourne
	//retourne null si aucun token n'était présent
	private function getTokenFromSession()
	{
		if (!empty($_SESSION['token'])) {
			$sessionToken = $_SESSION['token'];
		} else {
			$sessionToken = "null";
		}
		return $sessionToken;
	}

	//stocke le token dans la session
	private function saveTokenInSession($newToken)
	{
		$_SESSION['token'] = $newToken;
	}

	//supprime le token de la session
	private function deleteTokenInSession()
	{
		unset($_SESSION['token']);
	}
}
