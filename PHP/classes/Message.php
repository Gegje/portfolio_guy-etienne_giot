<?php

class Message
{
    private $id;
    private $subject;
    private $content;
    private $postDate;
    private $idContact;

    //hydratation d'un nouveau message quand le formulaire est soumis
    public function __construct($subject, $content, $idContact)
    {
        //rècupère la date et l'heure courante
        $now = new DateTime();

        $this->setSubject($subject);
        $this->setContent($content);
        $this->setPostDate($now->format("d-m-Y à H:i"));
        $this->setIdContact($idContact);

        //envoie vers bdd
        $manager=new Manager;
        $manager->saveNewMessage($this);
    }

    //définition des getters et setters
    public function getId()
    {
        return $this->id;
    }


    public function getSubject()
    {
        return $this->subject;
    }

    private function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }


    public function getContent()
    {
        return $this->content;
    }

    private function setContent($content)
    {
        $this->content = $content;

        return $this;
    }


    public function getPostDate()
    {
        return $this->postDate;
    }

    private function setPostDate($postDate)
    {
        $this->postDate = $postDate;

        return $this;
    }


    public function getIdContact()
    {
        return $this->idContact;
    }

    private function setIdContact($idContact)
    {
        $this->idContact = $idContact;

        return $this;
    }
}