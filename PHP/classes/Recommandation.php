<?php

class Recommandation
{
    private $id;
    private $entreprise;
    private $content;
    private $postDate;
    private $idContact;

    //hydrate $recommandation quand le formulaire est soumis
    public function __construct($entreprise, $content, $idContact)
    {
        //rècupère la date et l'heure courante
        $now = new DateTime();

        $this->setEntreprise($entreprise);
        $this->setContent($content);
        $this->setPostDate($now->format("d-m-Y à H:i"));
        $this->setIdContact($idContact);

        //envoie pour enregistrement en bdd
        $manager=new Manager;
        $manager->saveNewRecommandation($this);
    }

    //getters et setters
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getEntreprise()
    {
        return $this->entreprise;
    }

    public function setEntreprise($entreprise)
    {
        $this->entreprise = $entreprise;

        return $this;
    }


    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

 
    public function getPostDate()
    {
        return $this->postDate;
    }

    public function setPostDate($postDate)
    {
        $this->postDate = $postDate;

        return $this;
    }


    public function getIdContact()
    {
        return $this->idContact;
    }
 
    public function setIdContact($idContact)
    {
        $this->idContact = $idContact;

        return $this;
    }
}