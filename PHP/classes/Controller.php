<?php

class Controller
{
    //affichage de la page d'accueil
    public function home()
    {
        $manager = new Manager;

        //récupère les contenus de la page
        $contents = $manager->getContentsByTitle('home');
        //récupère la citation de la page
        $quote = $manager->getQuoteByTitle('home');

        include("templates/home.php");
    }


    //affichage des cgu et mentions rgpd
    public function cgu()
    {
        $manager = new Manager;

        if ($_SESSION['lang'] == 'fr') {
            $_GET['title'] = 'CGU-RGPD';
        } else {
            $_GET['title'] = 'GCU-GPDR';
        }

        $quote = $manager->getQuoteByTitle('cgu');

        include("templates/cgu.php");
    }


    //affichage des pages de contenu de premier niveau
    public function content()
    {
        $title = $_GET['title'];
        $manager = new Manager;

        //récupère les contenus à afficher
        $contents = $manager->getContentsByTitle($title);
        if (!empty($contents)) {
            $quote = $manager->getQuoteByTitle($title);
        } else {
            header('location: index.php?page=404');
            die();
        }

        include("templates/default.php");
    }

    //affichage des pages de détails
    public function details()
    {
        $title = $_GET['title'];
        $manager = new Manager;

        //récupère les contenus à afficher
        $contents = $manager->getContentsByTitle($title);
        if (!empty($contents)) {
            include("templates/details.php");
        } else {
            header('location: index.php?page=404');
            die();
        }
    }

        //affichage du rapport de stage
        public function stage()
        {
            $title = $_GET['page'];
            $manager = new Manager;
    
            //récupère les contenus à afficher
            $contents = $manager->getContentsByTitle($title);
            if (!empty($contents)) {
                include("templates/stage.php");
            } else {
                header('location: index.php?page=404');
                die();
            }
        }

    //affichage des pages "recommandations" et "contact", traitement des formulaires
    public function contacts()
    {
        $title = $_GET['title'];
        $manager = new Manager;

        //on instancie CsrfProtector donc crée un token
        $csrfProtector = new CsrfProtector;

        //si le formulaire est soumis
        if (FormValidation::formIsSubmitted() == true) {
            //on instancie FormValidation
            $formValidation = new FormValidation;

            //pour la validation,il est possible de boucler en foreach sur les champs
            //mais avec l'obligation de tester à chaque fois le champ pour définir les longueurs min et max
            //le code est plus long et ça me parait moins performant

            //on récupère et nettoie le nom (pour éviter notamment des attaques XSS)
            $name = $formValidation->purifyString($_POST['name']);
            //on teste la présence et la longueur (minimum défini à 3, maximum à 50) du nom
            $formValidation->basicValidation($name, 'name', 2, 50);

            //on récupère et nettoie l'email
            $email = $formValidation->purifyString($_POST['email']);
            //on teste la présence et la longueur (minimum défini à 6, maximum à 50) d'un email
            $formValidation->basicValidation($email, 'email', 6, 50);
            //et sa validité
            $formValidation->validateEmail($email, 'email');

            //on récupère, nettoie et valide le nom de l'entreprise ou l'objet du message (selon le cas)
            if ($title == 'recommandations') {
                $entreprise = $formValidation->purifyString($_POST['entreprise']);
                $formValidation->basicValidation($entreprise, 'entreprise', 1, 50);
            } else {
                $subject = $formValidation->purifyString($_POST['subject']);
                $formValidation->basicValidation($subject, 'subject', 1, 50);
            }

            //on récupère, nettoie et teste le contenu du message
            $content = $formValidation->purifyString($_POST['content']);
            $formValidation->basicValidation($content, 'content', 3, 500);

            //teste le captcha
            $formValidation->validateCaptcha($_POST['g-recaptcha-response']);
            
            //teste le token anti-csrf
            $formValidation->validateCsrfToken($_POST['token'], $csrfProtector);

            //on récupère le statut de validité du formulaire
            $formIsValid = $formValidation->formIsValid();

            //s'il est valide
            if ($formIsValid == true) {
                //crée un nouveau contact
                $contact = new Contact($name, $email);
                //et l'enregistre dans la base de données en récupérant son Id
                $idContact = $manager->saveNewContact($contact);

                //s'il s'agit d'une recommandation
                if ($title == 'recommandations') {
                    //crée et enregistre la recommandation en base de données
                    $recommandation = new Recommandation($entreprise, $content, $idContact);

                    //m'envoie un email pour information et vérification
                    $mailer = new Mailer();
                    $mailer->sendRecommandationForValidation($name, $email, $content);
                //sinon
                } else {
                    //crée et enregistre un nouveau message
                    $message = new Message($subject, $content, $idContact);

                    //m'envoie un email (et un email de confirmation à l'expéditeur)
                    $mailer = new Mailer();
                    $mailer->sendNewMail($name, $email, $subject, $content, $formValidation);
                }
                //récupère le tableau d'erreurs
                $errors = $formValidation->getErrors();
                unset($_POST);
            }
        }


        //si on est sur la page recommandations
        if ($title == 'recommandations') {
            //récupère les recommandations à afficher
            $recommandations = $manager->getRecommandations();
        }

        $quote = $manager->getQuoteByTitle($title);

        include("templates/contacts.php");
    }


    //affiche la page d'erreur 404
    public function fourofour()
    {
        include("templates/404.php");
    }
}
