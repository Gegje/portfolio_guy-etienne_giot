<?php

class FormValidation extends Validator
{
    //retourne un booléen, true si un formulaire a été soumis
    public static function formIsSubmitted()
    {
        if (!empty($_POST)) {
            return true;
        }
        return false;
    }

    //vérifie que le champ spécifié est bien rempli
    public function validateRequired($value, $fieldName)
    {
        if (empty($value)) {
            if ($_SESSION['lang'] == 'fr') {
                $this->addError($fieldName, "Le champ $fieldName est requis !");
            } else {
                $this->addError($fieldName, "The field $fieldName is required!");
            }
        }
    }


    //vérifie que la longueur de l'entrée correspond aux attentes
    public function validateStringLength($string, $fieldName, $min, $max)
    {
        if (strlen($string) < $min) {
            if ($_SESSION['lang'] == 'fr') {
                $this->addError($fieldName, "Le champ $fieldName doit contenir au moins $min caractères !");
            } else {
                $this->addError($fieldName, "There should be at least $min characters in the field $fieldName");
            }
        } elseif (strlen($string) > $max) {
            if ($_SESSION['lang'] == 'fr') {
                $this->addError($fieldName, "Le champ $fieldName ne peut contenir plus de $max caractères ! Veuillez raccourcir!");
            } else {
                $this->addError($fieldName, "There can't be more than $max characters in the field $fieldName ! Please shorten!");
            }
        }
    }


    //validation de base des différents champs: existence et longueur
    public function basicValidation($string, $fieldName, $min, $max)
    {
        $this->validateRequired($string, $fieldName);
        $this->validateStringLength($string, $fieldName, $min, $max);
    }


    //vérifie que l'email donné est au bon format
    //(même si une première vérification est réalisée par le champ "email", le purifyString peut avoir modifié la valeur)
    public function validateEmail($email, $fieldName)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            if ($_SESSION['lang'] == 'fr') {
                $this->addError($fieldName, "L'adresse que vous avez donnée n'est pas valide !");
            } else {
                $this->addError($fieldName, "You entered an invalid email address!");
            }
        }
    }


    //validation du token anti-csrf
    public function validateCsrfToken($csrfToken, $csrfProtector)
    {
        //teste la validité du token
        $matchingTokens = $csrfProtector->validateReceivedToken($csrfToken);
        if ($matchingTokens == false) {
            if ($_SESSION['lang'] == 'fr') {
                $this->addError('token', "votre identité ne peut être validée! Envoi refusé");
            } else {
                $this->addError('token', "your identity couldn't be verified! Your message has not been sent.");
            }
        }
    }

    //validation du captcha
    public function validateCaptcha($response)
    {
        // Ma clé privée
        $secret = "6Le0-doUAAAAANfN4lHSR0S-pyNxlyCHMBAo_taf";
        // On récupère l'IP de l'utilisateur
        $remoteip = $_SERVER['REMOTE_ADDR'];

        $api_url = "https://www.google.com/recaptcha/api/siteverify?secret="
            . $secret
            . "&response=" . $response
            . "&remoteip=" . $remoteip;

        $decode = json_decode(file_get_contents($api_url), true);

        if ($decode['success'] == false) {
            // C'est un robot ou le code de vérification est incorrecte
            $this->addError('g-recaptcha-response','le captcha n\'est pas validé');
        }
    }


    //vérifie le bon envoi de l'email
    public function validateEmailSent($mailingError)
    {
        if (!empty($mailingError)) {
            if ($_SESSION['lang'] == 'fr') {
                $this->addError('mailer', "votre message n'a pu être envoyé. Mailer Error : '.$mailingError.'");
            } else {
                $this->addError('mailer', "Your message couldn't be sent. Mailer Error : '.$mailingError.'");
            }
        }
    }
}
