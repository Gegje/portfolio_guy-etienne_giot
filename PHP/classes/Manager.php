<?php
//gère l'ensemble des requêtes à la base de données
class Manager
{
    public function getContentsByTitle($title)
    {
        //connexion à la base de données
        $pdo = DbConnection::getPdo();

        //récupère la langue en cours
        $lang = $_SESSION['lang'];

        //récupère les contenus à afficher dans les pages "default" et "details"
        $sql = "SELECT * FROM contents WHERE page_Title = '$title' AND lang = '$lang' ORDER BY position";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $contents = $stmt->fetchAll();
        return $contents;
    }


    public function getQuoteByTitle($title)
    {
        $pdo = DbConnection::getpdo();
        //récupère la langue en cours
        $lang = $_SESSION['lang'];


        //récupère les citations à afficher
        $sql = "SELECT content FROM quotes WHERE page_title = '$title' AND lang = '$lang'";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $quote = $stmt->fetch();
        return $quote;
    }


    public function saveNewContact($contact)
    {
        //connection à la base de données
        $pdo = DbConnection::getPdo();

        //vérifie si le contact existe déjà en bdd
        $email = $contact->getEmail();
        $getId = $this->checkContactId($email, $pdo);
        //si ce n'est pas le cas, on remplit la table contacts
        if (!$getId) {
            $sql = "INSERT INTO contacts (name, email)
                    VALUES (:name, :email)";
            $stmt = $pdo->prepare($sql);
            $stmt->execute([
                ":name" => $contact->getName(),
                ":email" => $email,
            ]);

            // pour récupérer l'id créée
            $idContact = $pdo->lastInsertId();
        } //sinon on récupère son Id
        else {
            $idContact = $getId['id'];
        }
        return $idContact;
    }

    //vérifie que le contact n'est pas déjà enregistré
    private function checkContactId($email, $pdo)
    {
        $sql = "SELECT id FROM contacts WHERE email = :email";
            $stmt = $pdo->prepare($sql);
            $stmt->execute([
                ":email"=>$email
            ]);
            return $stmt->fetch();
    }


    //enregistre une nouvelle recommandation en base de données
    public function saveNewRecommandation($recommandation)
    {
        //connection à la base de données
        $pdo = DbConnection::getPdo();

        //vérifie si la personne a déjà déposé une recommandation (on n'en veut qu'une par personne)
        $checkId = $this->checkPreviousRecommandation($recommandation->getIdContact(), $pdo);
        //si ce n'est pas le cas, on enregistre tout
        if (empty($checkId)) {
            $sql = "INSERT INTO recommandations (entreprise, content, post_date, id_Contacts)
                    VALUES (:entreprise, :content, :postDate, :idContact)";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            ":entreprise" => $recommandation->getEntreprise(),
            ":content" => $recommandation->getContent(),
            ":postDate" => $recommandation->getPostDate(),
            ":idContact" => $recommandation->getIdContact(),
        ]);
        } //sinon, on se contente de mettre à jour
        else {
            $sql = "UPDATE recommandations SET content=:content, post_date= :postDate  WHERE id_Contacts= :contactId";
            $stmt = $pdo->prepare($sql);
            $stmt->execute([
                ":content" => $recommandation->getContent(),
                ":postDate" =>$recommandation->getPostDate(),
                ":contactId"=>$recommandation->getIdContact(),
            ]);
        }
    }

    //vérifie si la personne a déjà déposé une recommandation
    private function checkPreviousRecommandation($idContact, $pdo)
    {
        $sql = "SELECT id_Contacts FROM recommandations WHERE id_Contacts=:idContact";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            ":idContact" => $idContact,
        ]);
        return $stmt->fetch();
    }


    public function saveNewMessage($message)
    {
        //connection à la base de données
        $pdo = DbConnection::getPdo();

        $sql = "INSERT INTO messages (subject, content, post_date, id_Contacts)
                    VALUES (:subject, :content, :postDate, :idContact)";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            ":subject" => $message->getSubject(),
            ":content" => $message->getContent(),
            ":postDate" => $message->getPostDate(),
            ":idContact" => $message->getIdContact(),
        ]);
    }


    public function getRecommandations()
    {
        $pdo = DbConnection::getPdo();
        //récupère les données à afficher dans la table recommandation
        $sql = "SELECT c.name, r.entreprise, r.content, r.post_date 
            FROM recommandations AS r JOIN contacts AS c ON c.id=r.id_Contacts ORDER BY post_date DESC";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $recommandations = $stmt->fetchALL();
        return $recommandations;
    }

    public function saveNewVisit($visit)
    {
        //requête sql pour insérer les données dans la bdd
        $sql = "INSERT INTO visits (page_name, visitor_IP, visitor_user_agent, date)
            VALUES (:page, :visitorIP, :visitorUserAgent, :dateTime)";

        //création de la connexion
        $pdo = DbConnection::getPdo();

        //traitement de la requête sql
        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            ":page"=>$visit->getPage(),
            ":visitorIP"=>$visit->getVisitorIP(),
            ":visitorUserAgent"=>$visit->getVisitorUserAgent(),
            ":dateTime"=>$visit->getDateTime(),
        ]);
    }

    //retourne le nombre de visites et de visiteurs pour la page $page
    public static function countVisits($page)
    {
        //requête sql pour récupérer les données pour la page concernée
        $sql = "SELECT COUNT(id) as count, COUNT(DISTINCT(visitor_IP)) as nbIP FROM visits WHERE page_name = '$page'";

        //connexion
        $pdo = DbConnection::getPdo();

        //exécution de la requête sql
        $stmt = $pdo->prepare($sql);
        $stmt -> execute();

        //récupère les résultats de la requête
        $visitsCount = $stmt -> fetch();
        //retourne le nombre de visites calculé et le nombre de visiteurs
        return $visitsCount;
    }
}
