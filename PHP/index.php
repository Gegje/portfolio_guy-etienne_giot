<?php

//on est dans le contrôleur frontal \o/
//ce fichier reçoit toutes les requêtes au site

//on commence par inclure toutes nos classes ! 
spl_autoload_register(function ($className) {
    //si le fichier existe dans le dossier classes... 
    if (file_exists("classes/$className.php")) {
        //on l'include
        include("classes/$className.php");
    }
});

//autoload des fichiers "vendor", ici pour PHPMAILER
require("vendor/autoload.php");

//inclusion des constantes de connection en BDD
include('templates/DB.php');

//on initie la session
session_start();

//définit la page par défaut en 'home'
if (empty($_GET['page'])) {
    $_GET['page'] = 'home';
}

//récupère et stocke la langue en cours
if (empty($_GET['lang'])){
    $_GET['lang'] = 'fr';
}
$_SESSION['lang'] = $_GET['lang'];

//instancie le contrôleur
$controller = new Controller();

//envoie vers les pages demandées
if ($_GET['page'] == 'home') {
    $controller->home();
}elseif ($_GET['page'] == 'cgu') {
    $controller->cgu();
} elseif ($_GET['page'] == 'default') {
    $controller->content();
} elseif ($_GET['page'] == 'details') {
    $controller->details();
} elseif ($_GET['page'] == 'stage') {
    $controller->stage();
}elseif ($_GET['page'] == 'contacts') {
    $controller->contacts();
} 
//si on n'a pas trouvé la page, alors c'est une erreur 404 ! 
else {
    $_GET['page'] = '404';
    $controller->fourofour();
}
