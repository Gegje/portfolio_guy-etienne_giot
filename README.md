Ceci est la version en PHP orienté Objet de mon portfolio.
Elle utilise en grande partie le travail qui a été fait précédemment pour la version en PHP procédural, notamment le maquettage et la base visuelle mais aussi une parite des fonctionnalités qui seront développées ici.


Le projet initial:
Création d'un portfolio/blog permettant de mettre en avant les compétences acquises ou en cours d'acquisition, les expériences passées et en cours afin de le présenter à de potentiels recruteurs.
On trouvera les maquettes graphiques dans le dossier "Maquettage", les outils de gestion de projet et les fonctionnalités dans le dossier "Outils de gestion de projet".
Le portfolio numérique lui-même se trouve dans le dossier "Html".
Ce projet est réalisé par Guy-Etienne Giot pour son usage personnel. Le code est disponible pour autant qu'il intéresse quiconque mais certaines images ne sont pas libres de droits (j'en ai acheté les droits pour mon usage personnel). Merci de ne pas les utiliser en dehors de ce portfolio.